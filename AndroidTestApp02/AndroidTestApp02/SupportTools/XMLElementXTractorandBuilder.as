package com.fsdcontentengine.xmldataprocessor {

	import flash.display.*;
	import flash.events.*;
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	import fl.transitions.easing.*;

	public class XMLElementXTractorandBuilder {

		var elementData:XML, stageobj:Object, lineToPlaceElement:Line, QuestionGenrationMode:Boolean;
		public var ElementType:String="";

		function XMLElementXTractorandBuilder(elementXML:XML, stageObject:Object, lineInst:Line){
			elementData = elementXML;
			stageobj = stageObject;
			lineToPlaceElement = lineInst;
			ElementType = elementData.attribute("type");
		}

		//THIS METHOD RETURNS "none" IF THE CALL IS MADE FROM "ISMQuestionBoard" AS WE DONOT WANT TO ANIMATE DURING QUESTIONS ASKING
		private function getGlobalTweenStyle():String{
			if(QuestionGenrationMode == true){
				return "none";
			}else{
				return "";
			}
		}
		private function getBooleanValue(inString:String):Boolean{
			if ( inString == "true"){
				return true;
			}else{
				return false;
			}
		}
		private function getDefaultValue(inString:String):Object{
			if(inString == "String"){
				return "";
			}else if(inString == "int"){
				return 0;
			}else if(inString == "Boolean"){
				return false;
			}
			return "";
		}

		public function ProcessElements():Object{
			var elementType:String = "", createdObj:Object;
			elementType = elementData.attribute("type");
			switch(elementType){
				case "txtContent":
					createdObj = Add_txtContent();
					break;
				case "textArea":
					createdObj = Add_textArea();
					break;
				case "variable":
					createdObj = Add_variable();
					break;
				case "tableContent":
					createdObj = Add_tableContent();
					break;
				case "fraction_level1_1":
					createdObj = Add_fraction_level1_1();
					break;
				case "operator":
					createdObj = Add_operator();
					break;
				case "over_head_divide":
					createdObj = Add_over_head_divide();
					break;
				case "free_step_divide":
					createdObj = Add_free_step_divide();
					break;
				case "dividedLine":
					createdObj = Add_dividedLine();
					break;
				case "upArc":
					createdObj = Add_upArc();
					break;
				case "downArc":
					createdObj = Add_downArc();
					break;
				case "dyn_cov_polynomial_1":
					createdObj = Add_dyn_cov_polynomial_1();
					break;
				case "cov_polynomial_1":
					createdObj = Add_cov_polynomial_1();
					break;
				case "cov_polynomial_2":
					createdObj = Add_cov_polynomial_2();
					break;
				case "cov_polynomial_3":
					createdObj = Add_cov_polynomial_3();
					break;
				case "cov_polynomial_4":
					createdObj = Add_cov_polynomial_4();
					break;
				case "plainLine":
					createdObj = Add_plainLine();
					break;
				case "plainCircle":
					createdObj = Add_plainCircle();
					break;
				case "plainSquare":
					createdObj = Add_plainSquare();
					break;
				case "squareBlocks":
					createdObj = Add_squareBlocks();
					break;
				case "plainTriangle":
					createdObj = Add_plainTriangle();
					break;
				case "plainEllipse":
					createdObj = Add_plainEllipse();
					break;
				case "plainPloygon15":
					createdObj = Add_plainPloygon15();
					break;
				case "plainArc":
					createdObj = Add_plainArc();
					break;
				case "segmentedCircle":
					createdObj = Add_segmentedCircle();
					break;
				case "segmentedSquare":
					createdObj = Add_segmentedSquare();
					break;
				case "graphBaseSheet":
					createdObj = Add_graphBaseSheet();
					break;
				case "graphPlainLine":
					createdObj = Add_graphPlainLine();
					break;
				case "graphPlainSquare":
					createdObj = Add_graphPlainSquare();
					break;
				case "graphPlainTriangle":
					createdObj = Add_graphPlainTriangle();
					break;
				case "graphPlainCircle":
					createdObj = Add_graphPlainCircle();
					break;
				case "graphPlainEllipse":
					createdObj = Add_graphPlainEllipse();
					break;
				case "graphPlainArc":
					createdObj = Add_graphPlainArc();
					break;
				case "ThreeDCube":
					createdObj = Add_ThreeDCube();
					break;
				case "ThreeDSphere":
					createdObj = Add_ThreeDSphere();
					break;
				case "NewFeature":
					createdObj = Add_NewFeature();
					break;
				case "voiceLink":
					createdObj = Add_voiceLink();
					break;
				case "highlightBox":
					createdObj = Add_highlightBox();
					break;
				case "operateExistingControl":
					createdObj = Add_operateExistingControl();
					break;
				case "multiStepLine":
					createdObj = Add_multiStepLine();
					break;
				case "executeMultiStep":
					createdObj = Add_executeMultiStep();
					break;
				case "executeMultiStepHH":
					createdObj = Add_executeMultiStepHH();
					break;
				default:
					break;
			}
			return createdObj;
		}



		public function Add_txtContent():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,labelContent:String,styleBold:Boolean,styleUnderLine:Boolean,styleItalics:Boolean,StyleColor:String,hilightColor:String,rotationAngle:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			labelContent= (processingXML.labelContent.children().length() > 0) ? String(processingXML.labelContent) :  String(getDefaultValue("String"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			styleBold=  (processingXML.styleBold.children().length() > 0)  ?  getBooleanValue(processingXML.styleBold) : getBooleanValue("false");
			styleUnderLine=  (processingXML.styleUnderLine.children().length() > 0)  ?  getBooleanValue(processingXML.styleUnderLine) : getBooleanValue("false");
			styleItalics=  (processingXML.styleItalics.children().length() > 0)  ?  getBooleanValue(processingXML.styleItalics) : getBooleanValue("false");
			StyleColor= (processingXML.StyleColor.children().length() > 0) ? String(processingXML.StyleColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			rotationAngle= (processingXML.rotationAngle.children().length() > 0) ? int(processingXML.rotationAngle)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var txtContent_inst_1:txtContent = lineToPlaceElement.Add_txtContent(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,labelContent,styleBold,styleUnderLine,styleItalics,StyleColor,hilightColor,rotationAngle,controlHeight);
			txtContent_inst_1.alpha = 0;
			stageobj.addChild(txtContent_inst_1);
			MovieClip(txtContent_inst_1).addEventListener(Event.ENTER_FRAME, txtContent_inst_1_AppearEvent);
			function txtContent_inst_1_AppearEvent(event:Event):void {
				if (txtContent_inst_1.alpha >= 1){
					txtContent_inst_1.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, txtContent_inst_1_AppearEvent);
				}else if(txtContent_inst_1.alpha  < 0.5 ){
					txtContent_inst_1.alpha = txtContent_inst_1.alpha + 0.40;
				}else{
					txtContent_inst_1.alpha = txtContent_inst_1.alpha + 0.35;
				}
			}
			return txtContent_inst_1;

		}



		public function Add_textArea():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,labelContent:String,styleBold:Boolean,styleUnderLine:Boolean,styleItalics:Boolean,StyleColor:String,controlWidth:int,hilightColor:String,rotationAngle:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			labelContent= (processingXML.labelContent.children().length() > 0) ? String(processingXML.labelContent) :  String(getDefaultValue("String"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			styleBold=  (processingXML.styleBold.children().length() > 0)  ?  getBooleanValue(processingXML.styleBold) : getBooleanValue("false");
			styleUnderLine=  (processingXML.styleUnderLine.children().length() > 0)  ?  getBooleanValue(processingXML.styleUnderLine) : getBooleanValue("false");
			styleItalics=  (processingXML.styleItalics.children().length() > 0)  ?  getBooleanValue(processingXML.styleItalics) : getBooleanValue("false");
			StyleColor= (processingXML.StyleColor.children().length() > 0) ? String(processingXML.StyleColor)  :  String(getDefaultValue("String"));
			controlWidth= (processingXML.controlWidth.children().length() > 0) ? int(processingXML.controlWidth)  :  int(getDefaultValue("int"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			rotationAngle= (processingXML.rotationAngle.children().length() > 0) ? int(processingXML.rotationAngle)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var textArea_inst_2:textArea = lineToPlaceElement.Add_textArea(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,labelContent,styleBold,styleUnderLine,styleItalics,StyleColor,controlWidth,hilightColor,rotationAngle,controlHeight);
			textArea_inst_2.alpha = 0;
			stageobj.addChild(textArea_inst_2);
			MovieClip(textArea_inst_2).addEventListener(Event.ENTER_FRAME, textArea_inst_2_AppearEvent);
			function textArea_inst_2_AppearEvent(event:Event):void {
				if (textArea_inst_2.alpha >= 1){
					textArea_inst_2.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, textArea_inst_2_AppearEvent);
				}else if(textArea_inst_2.alpha  < 0.5 ){
					textArea_inst_2.alpha = textArea_inst_2.alpha + 0.40;
				}else{
					textArea_inst_2.alpha = textArea_inst_2.alpha + 0.35;
				}
			}
			return textArea_inst_2;

		}



		public function Add_variable():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,var1:String,var1subscript:String,var1indice:String,postVariable:String,hilightColor:String,rotationAngle:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			var1= (processingXML.var1.children().length() > 0) ? String(processingXML.var1) :  String(getDefaultValue("String"));
			var1subscript= (processingXML.var1subscript.children().length() > 0) ? String(processingXML.var1subscript) :  String(getDefaultValue("String"));
			var1indice= (processingXML.var1indice.children().length() > 0) ? String(processingXML.var1indice) :  String(getDefaultValue("String"));
			postVariable= (processingXML.postVariable.children().length() > 0) ? String(processingXML.postVariable) :  String(getDefaultValue("String"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			rotationAngle= (processingXML.rotationAngle.children().length() > 0) ? int(processingXML.rotationAngle)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var variable_inst_3:variable = lineToPlaceElement.Add_variable(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,var1,var1subscript,var1indice,postVariable,hilightColor,rotationAngle,controlHeight);
			variable_inst_3.alpha = 0;
			stageobj.addChild(variable_inst_3);
			MovieClip(variable_inst_3).addEventListener(Event.ENTER_FRAME, variable_inst_3_AppearEvent);
			function variable_inst_3_AppearEvent(event:Event):void {
				if (variable_inst_3.alpha >= 1){
					variable_inst_3.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, variable_inst_3_AppearEvent);
				}else if(variable_inst_3.alpha  < 0.5 ){
					variable_inst_3.alpha = variable_inst_3.alpha + 0.40;
				}else{
					variable_inst_3.alpha = variable_inst_3.alpha + 0.35;
				}
			}
			return variable_inst_3;

		}



		public function Add_tableContent():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,tableStructureDefinition:String,tableContents:String,styleColor:String,hilightColor:String,rotationAngle:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			tableStructureDefinition= (processingXML.tableStructureDefinition.children().length() > 0) ? String(processingXML.tableStructureDefinition) :  String(getDefaultValue("String"));
			tableContents= (processingXML.tableContents.children().length() > 0) ? String(processingXML.tableContents) :  String(getDefaultValue("String"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			rotationAngle= (processingXML.rotationAngle.children().length() > 0) ? int(processingXML.rotationAngle)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var tableContent_inst_4:tableContent = lineToPlaceElement.Add_tableContent(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,tableStructureDefinition,tableContents,styleColor,hilightColor,rotationAngle,controlHeight);
			tableContent_inst_4.alpha = 0;
			stageobj.addChild(tableContent_inst_4);
			MovieClip(tableContent_inst_4).addEventListener(Event.ENTER_FRAME, tableContent_inst_4_AppearEvent);
			function tableContent_inst_4_AppearEvent(event:Event):void {
				if (tableContent_inst_4.alpha >= 1){
					tableContent_inst_4.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, tableContent_inst_4_AppearEvent);
				}else if(tableContent_inst_4.alpha  < 0.5 ){
					tableContent_inst_4.alpha = tableContent_inst_4.alpha + 0.40;
				}else{
					tableContent_inst_4.alpha = tableContent_inst_4.alpha + 0.35;
				}
			}
			return tableContent_inst_4;

		}



		public function Add_fraction_level1_1():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,var1:String,var2:String,paranthesis:Boolean,styleColor:String,hilightColor:String,rotationAngle:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			var1= (processingXML.var1.children().length() > 0) ? String(processingXML.var1) :  String(getDefaultValue("String"));
			var2= (processingXML.var2.children().length() > 0) ? String(processingXML.var2) :  String(getDefaultValue("String"));
			paranthesis=  (processingXML.paranthesis.children().length() > 0)  ? getBooleanValue(processingXML.paranthesis) : getBooleanValue("false"); 
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			rotationAngle= (processingXML.rotationAngle.children().length() > 0) ? int(processingXML.rotationAngle)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var fraction_level1_1_inst_5:fraction_level1_1 = lineToPlaceElement.Add_fraction_level1_1(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,var1,var2,paranthesis,styleColor,hilightColor,rotationAngle,controlHeight);
			fraction_level1_1_inst_5.alpha = 0;
			stageobj.addChild(fraction_level1_1_inst_5);
			MovieClip(fraction_level1_1_inst_5).addEventListener(Event.ENTER_FRAME, fraction_level1_1_inst_5_AppearEvent);
			function fraction_level1_1_inst_5_AppearEvent(event:Event):void {
				if (fraction_level1_1_inst_5.alpha >= 1){
					fraction_level1_1_inst_5.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, fraction_level1_1_inst_5_AppearEvent);
				}else if(fraction_level1_1_inst_5.alpha  < 0.5 ){
					fraction_level1_1_inst_5.alpha = fraction_level1_1_inst_5.alpha + 0.40;
				}else{
					fraction_level1_1_inst_5.alpha = fraction_level1_1_inst_5.alpha + 0.35;
				}
			}
			return fraction_level1_1_inst_5;

		}



		public function Add_operator():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,opr1:String,opr2:String,operatorHeight:int,operatorLength:int,styleColor:String,hilightColor:String,rotationAngle:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			opr1= (processingXML.opr1.children().length() > 0) ? String(processingXML.opr1) :  String(getDefaultValue("String"));
			opr2= (processingXML.opr2.children().length() > 0) ? String(processingXML.opr2) :  String(getDefaultValue("String"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			operatorHeight= (processingXML.operatorHeight.children().length() > 0) ? int(processingXML.operatorHeight)  :  int(getDefaultValue("int"));
			operatorLength= (processingXML.operatorLength.children().length() > 0) ? int(processingXML.operatorLength)  :  int(getDefaultValue("int"));
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			rotationAngle= (processingXML.rotationAngle.children().length() > 0) ? int(processingXML.rotationAngle)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var operator_inst_6:operator = lineToPlaceElement.Add_operator(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,opr1,opr2,operatorHeight,operatorLength,styleColor,hilightColor,rotationAngle,controlHeight);
			operator_inst_6.alpha = 0;
			stageobj.addChild(operator_inst_6);
			MovieClip(operator_inst_6).addEventListener(Event.ENTER_FRAME, operator_inst_6_AppearEvent);
			function operator_inst_6_AppearEvent(event:Event):void {
				if (operator_inst_6.alpha >= 1){
					operator_inst_6.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, operator_inst_6_AppearEvent);
				}else if(operator_inst_6.alpha  < 0.5 ){
					operator_inst_6.alpha = operator_inst_6.alpha + 0.40;
				}else{
					operator_inst_6.alpha = operator_inst_6.alpha + 0.35;
				}
			}
			return operator_inst_6;

		}



		public function Add_over_head_divide():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,verticalLength:int,horizontalLength:int,styleColor:String,hilightColor:String,rotationAngle:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			verticalLength= (processingXML.verticalLength.children().length() > 0) ? int(processingXML.verticalLength) :  int(getDefaultValue("int"));
			horizontalLength= (processingXML.horizontalLength.children().length() > 0) ? int(processingXML.horizontalLength) :  int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			rotationAngle= (processingXML.rotationAngle.children().length() > 0) ? int(processingXML.rotationAngle)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var over_head_divide_inst_7:over_head_divide = lineToPlaceElement.Add_over_head_divide(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,verticalLength,horizontalLength,styleColor,hilightColor,rotationAngle,controlHeight);
			over_head_divide_inst_7.alpha = 0;
			stageobj.addChild(over_head_divide_inst_7);
			MovieClip(over_head_divide_inst_7).addEventListener(Event.ENTER_FRAME, over_head_divide_inst_7_AppearEvent);
			function over_head_divide_inst_7_AppearEvent(event:Event):void {
				if (over_head_divide_inst_7.alpha >= 1){
					over_head_divide_inst_7.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, over_head_divide_inst_7_AppearEvent);
				}else if(over_head_divide_inst_7.alpha  < 0.5 ){
					over_head_divide_inst_7.alpha = over_head_divide_inst_7.alpha + 0.40;
				}else{
					over_head_divide_inst_7.alpha = over_head_divide_inst_7.alpha + 0.35;
				}
			}
			return over_head_divide_inst_7;

		}



		public function Add_free_step_divide():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,verticalLength:int,horizontalLength:int,styleColor:String,hilightColor:String,rotationAngle:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			verticalLength= (processingXML.verticalLength.children().length() > 0) ? int(processingXML.verticalLength) :  int(getDefaultValue("int"));
			horizontalLength= (processingXML.horizontalLength.children().length() > 0) ? int(processingXML.horizontalLength) :  int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			rotationAngle= (processingXML.rotationAngle.children().length() > 0) ? int(processingXML.rotationAngle)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var free_step_divide_inst_8:free_step_divide = lineToPlaceElement.Add_free_step_divide(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,verticalLength,horizontalLength,styleColor,hilightColor,rotationAngle,controlHeight);
			free_step_divide_inst_8.alpha = 0;
			stageobj.addChild(free_step_divide_inst_8);
			MovieClip(free_step_divide_inst_8).addEventListener(Event.ENTER_FRAME, free_step_divide_inst_8_AppearEvent);
			function free_step_divide_inst_8_AppearEvent(event:Event):void {
				if (free_step_divide_inst_8.alpha >= 1){
					free_step_divide_inst_8.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, free_step_divide_inst_8_AppearEvent);
				}else if(free_step_divide_inst_8.alpha  < 0.5 ){
					free_step_divide_inst_8.alpha = free_step_divide_inst_8.alpha + 0.40;
				}else{
					free_step_divide_inst_8.alpha = free_step_divide_inst_8.alpha + 0.35;
				}
			}
			return free_step_divide_inst_8;

		}



		public function Add_dividedLine():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,Length:int,content:String,styleColor:String,textColor:String,hilightColor:String,rotationAngle:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			Length= (processingXML.Length.children().length() > 0) ? int(processingXML.Length) :  int(getDefaultValue("int"));
			content= (processingXML.content.children().length() > 0) ? String(processingXML.content) :  String(getDefaultValue("String"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			textColor= (processingXML.textColor.children().length() > 0) ? String(processingXML.textColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			rotationAngle= (processingXML.rotationAngle.children().length() > 0) ? int(processingXML.rotationAngle)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var dividedLine_inst_9:dividedLine = lineToPlaceElement.Add_dividedLine(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,Length,content,styleColor,textColor,hilightColor,rotationAngle,controlHeight);
			dividedLine_inst_9.alpha = 0;
			stageobj.addChild(dividedLine_inst_9);
			MovieClip(dividedLine_inst_9).addEventListener(Event.ENTER_FRAME, dividedLine_inst_9_AppearEvent);
			function dividedLine_inst_9_AppearEvent(event:Event):void {
				if (dividedLine_inst_9.alpha >= 1){
					dividedLine_inst_9.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, dividedLine_inst_9_AppearEvent);
				}else if(dividedLine_inst_9.alpha  < 0.5 ){
					dividedLine_inst_9.alpha = dividedLine_inst_9.alpha + 0.40;
				}else{
					dividedLine_inst_9.alpha = dividedLine_inst_9.alpha + 0.35;
				}
			}
			return dividedLine_inst_9;

		}



		public function Add_upArc():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,Length:int,bendHeight:int,styleColor:String,hilightColor:String,rotationAngle:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			Length= (processingXML.Length.children().length() > 0) ? int(processingXML.Length) :  int(getDefaultValue("int"));
			bendHeight= (processingXML.bendHeight.children().length() > 0) ? int(processingXML.bendHeight) :  int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			rotationAngle= (processingXML.rotationAngle.children().length() > 0) ? int(processingXML.rotationAngle)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var upArc_inst_10:upArc = lineToPlaceElement.Add_upArc(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,Length,bendHeight,styleColor,hilightColor,rotationAngle,controlHeight);
			upArc_inst_10.alpha = 0;
			stageobj.addChild(upArc_inst_10);
			MovieClip(upArc_inst_10).addEventListener(Event.ENTER_FRAME, upArc_inst_10_AppearEvent);
			function upArc_inst_10_AppearEvent(event:Event):void {
				if (upArc_inst_10.alpha >= 1){
					upArc_inst_10.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, upArc_inst_10_AppearEvent);
				}else if(upArc_inst_10.alpha  < 0.5 ){
					upArc_inst_10.alpha = upArc_inst_10.alpha + 0.40;
				}else{
					upArc_inst_10.alpha = upArc_inst_10.alpha + 0.35;
				}
			}
			return upArc_inst_10;

		}



		public function Add_downArc():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,Length:int,bendHeight:int,styleColor:String,hilightColor:String,rotationAngle:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			Length= (processingXML.Length.children().length() > 0) ? int(processingXML.Length) :  int(getDefaultValue("int"));
			bendHeight= (processingXML.bendHeight.children().length() > 0) ? int(processingXML.bendHeight) :  int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			rotationAngle= (processingXML.rotationAngle.children().length() > 0) ? int(processingXML.rotationAngle)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var downArc_inst_11:downArc = lineToPlaceElement.Add_downArc(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,Length,bendHeight,styleColor,hilightColor,rotationAngle,controlHeight);
			downArc_inst_11.alpha = 0;
			stageobj.addChild(downArc_inst_11);
			MovieClip(downArc_inst_11).addEventListener(Event.ENTER_FRAME, downArc_inst_11_AppearEvent);
			function downArc_inst_11_AppearEvent(event:Event):void {
				if (downArc_inst_11.alpha >= 1){
					downArc_inst_11.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, downArc_inst_11_AppearEvent);
				}else if(downArc_inst_11.alpha  < 0.5 ){
					downArc_inst_11.alpha = downArc_inst_11.alpha + 0.40;
				}else{
					downArc_inst_11.alpha = downArc_inst_11.alpha + 0.35;
				}
			}
			return downArc_inst_11;

		}



		public function Add_dyn_cov_polynomial_1():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,paranthesis1:String,var1:String,var1subscript:String,var1indice:String,postVariable:String,paranthesis2:String,paranthesisindice:String,styleColor:String,hilightColor:String,rotationAngle:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			paranthesis1= (processingXML.paranthesis1.children().length() > 0) ? String(processingXML.paranthesis1) :  String(getDefaultValue("String"));
			var1= (processingXML.var1.children().length() > 0) ? String(processingXML.var1) :  String(getDefaultValue("String"));
			var1subscript= (processingXML.var1subscript.children().length() > 0) ? String(processingXML.var1subscript) :  String(getDefaultValue("String"));
			var1indice= (processingXML.var1indice.children().length() > 0) ? String(processingXML.var1indice) :  String(getDefaultValue("String"));
			postVariable= (processingXML.postVariable.children().length() > 0) ? String(processingXML.postVariable) :  String(getDefaultValue("String"));
			paranthesis2= (processingXML.paranthesis2.children().length() > 0) ? String(processingXML.paranthesis2) :  String(getDefaultValue("String"));
			paranthesisindice= (processingXML.paranthesisindice.children().length() > 0) ? String(processingXML.paranthesisindice) :  String(getDefaultValue("String"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			rotationAngle= (processingXML.rotationAngle.children().length() > 0) ? int(processingXML.rotationAngle)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var dyn_cov_polynomial_1_inst_12:dyn_cov_polynomial_1 = lineToPlaceElement.Add_dyn_cov_polynomial_1(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,paranthesis1,var1,var1subscript,var1indice,postVariable,paranthesis2,paranthesisindice,styleColor,hilightColor,rotationAngle,controlHeight);
			dyn_cov_polynomial_1_inst_12.alpha = 0;
			stageobj.addChild(dyn_cov_polynomial_1_inst_12);
			MovieClip(dyn_cov_polynomial_1_inst_12).addEventListener(Event.ENTER_FRAME, dyn_cov_polynomial_1_inst_12_AppearEvent);
			function dyn_cov_polynomial_1_inst_12_AppearEvent(event:Event):void {
				if (dyn_cov_polynomial_1_inst_12.alpha >= 1){
					dyn_cov_polynomial_1_inst_12.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, dyn_cov_polynomial_1_inst_12_AppearEvent);
				}else if(dyn_cov_polynomial_1_inst_12.alpha  < 0.5 ){
					dyn_cov_polynomial_1_inst_12.alpha = dyn_cov_polynomial_1_inst_12.alpha + 0.40;
				}else{
					dyn_cov_polynomial_1_inst_12.alpha = dyn_cov_polynomial_1_inst_12.alpha + 0.35;
				}
			}
			return dyn_cov_polynomial_1_inst_12;

		}



		public function Add_cov_polynomial_1():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,paranthesis1:String,var1:String,var1subscript:String,var1indice:String,postVariable:String,paranthesis2:String,paranthesisindice:String,styleColor:String,hilightColor:String,rotationAngle:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			paranthesis1= (processingXML.paranthesis1.children().length() > 0) ? String(processingXML.paranthesis1) :  String(getDefaultValue("String"));
			var1= (processingXML.var1.children().length() > 0) ? String(processingXML.var1) :  String(getDefaultValue("String"));
			var1subscript= (processingXML.var1subscript.children().length() > 0) ? String(processingXML.var1subscript) :  String(getDefaultValue("String"));
			var1indice= (processingXML.var1indice.children().length() > 0) ? String(processingXML.var1indice) :  String(getDefaultValue("String"));
			postVariable= (processingXML.postVariable.children().length() > 0) ? String(processingXML.postVariable) :  String(getDefaultValue("String"));
			paranthesis2= (processingXML.paranthesis2.children().length() > 0) ? String(processingXML.paranthesis2) :  String(getDefaultValue("String"));
			paranthesisindice= (processingXML.paranthesisindice.children().length() > 0) ? String(processingXML.paranthesisindice) :  String(getDefaultValue("String"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			rotationAngle= (processingXML.rotationAngle.children().length() > 0) ? int(processingXML.rotationAngle)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var cov_polynomial_1_inst_13:cov_polynomial_1 = lineToPlaceElement.Add_cov_polynomial_1(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,paranthesis1,var1,var1subscript,var1indice,postVariable,paranthesis2,paranthesisindice,styleColor,hilightColor,rotationAngle,controlHeight);
			cov_polynomial_1_inst_13.alpha = 0;
			stageobj.addChild(cov_polynomial_1_inst_13);
			MovieClip(cov_polynomial_1_inst_13).addEventListener(Event.ENTER_FRAME, cov_polynomial_1_inst_13_AppearEvent);
			function cov_polynomial_1_inst_13_AppearEvent(event:Event):void {
				if (cov_polynomial_1_inst_13.alpha >= 1){
					cov_polynomial_1_inst_13.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, cov_polynomial_1_inst_13_AppearEvent);
				}else if(cov_polynomial_1_inst_13.alpha  < 0.5 ){
					cov_polynomial_1_inst_13.alpha = cov_polynomial_1_inst_13.alpha + 0.40;
				}else{
					cov_polynomial_1_inst_13.alpha = cov_polynomial_1_inst_13.alpha + 0.35;
				}
			}
			return cov_polynomial_1_inst_13;

		}



		public function Add_cov_polynomial_2():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,paranthesis1:String,var1:String,var1subscript:String,var1indice:String,postVariable1:String,operator1:String,var2:String,var2subscript:String,var2indice:String,postVariable2:String,paranthesis2:String,paranthesisindice:String,styleColor:String,hilightColor:String,rotationAngle:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			paranthesis1= (processingXML.paranthesis1.children().length() > 0) ? String(processingXML.paranthesis1) :  String(getDefaultValue("String"));
			var1= (processingXML.var1.children().length() > 0) ? String(processingXML.var1) :  String(getDefaultValue("String"));
			var1subscript= (processingXML.var1subscript.children().length() > 0) ? String(processingXML.var1subscript) :  String(getDefaultValue("String"));
			var1indice= (processingXML.var1indice.children().length() > 0) ? String(processingXML.var1indice) :  String(getDefaultValue("String"));
			postVariable1= (processingXML.postVariable1.children().length() > 0) ? String(processingXML.postVariable1) :  String(getDefaultValue("String"));
			operator1= (processingXML.operator1.children().length() > 0) ? String(processingXML.operator1) :  String(getDefaultValue("String"));
			var2= (processingXML.var2.children().length() > 0) ? String(processingXML.var2) :  String(getDefaultValue("String"));
			var2subscript= (processingXML.var2subscript.children().length() > 0) ? String(processingXML.var2subscript) :  String(getDefaultValue("String"));
			var2indice= (processingXML.var2indice.children().length() > 0) ? String(processingXML.var2indice) :  String(getDefaultValue("String"));
			postVariable2= (processingXML.postVariable2.children().length() > 0) ? String(processingXML.postVariable2) :  String(getDefaultValue("String"));
			paranthesis2= (processingXML.paranthesis2.children().length() > 0) ? String(processingXML.paranthesis2) :  String(getDefaultValue("String"));
			paranthesisindice= (processingXML.paranthesisindice.children().length() > 0) ? String(processingXML.paranthesisindice) :  String(getDefaultValue("String"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			rotationAngle= (processingXML.rotationAngle.children().length() > 0) ? int(processingXML.rotationAngle)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var cov_polynomial_2_inst_14:cov_polynomial_2 = lineToPlaceElement.Add_cov_polynomial_2(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,paranthesis1,var1,var1subscript,var1indice,postVariable1,operator1,var2,var2subscript,var2indice,postVariable2,paranthesis2,paranthesisindice,styleColor,hilightColor,rotationAngle,controlHeight);
			cov_polynomial_2_inst_14.alpha = 0;
			stageobj.addChild(cov_polynomial_2_inst_14);
			MovieClip(cov_polynomial_2_inst_14).addEventListener(Event.ENTER_FRAME, cov_polynomial_2_inst_14_AppearEvent);
			function cov_polynomial_2_inst_14_AppearEvent(event:Event):void {
				if (cov_polynomial_2_inst_14.alpha >= 1){
					cov_polynomial_2_inst_14.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, cov_polynomial_2_inst_14_AppearEvent);
				}else if(cov_polynomial_2_inst_14.alpha  < 0.5 ){
					cov_polynomial_2_inst_14.alpha = cov_polynomial_2_inst_14.alpha + 0.40;
				}else{
					cov_polynomial_2_inst_14.alpha = cov_polynomial_2_inst_14.alpha + 0.35;
				}
			}
			return cov_polynomial_2_inst_14;

		}



		public function Add_cov_polynomial_3():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,paranthesis1:String,var1:String,var1subscript:String,var1indice:String,postVariable1:String,operator1:String,var2:String,var2subscript:String,var2indice:String,postVariable2:String,operator2:String,var3:String,var3subscript:String,var3indice:String,postVariable3:String,paranthesis2:String,paranthesisindice:String,styleColor:String,hilightColor:String,rotationAngle:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			paranthesis1= (processingXML.paranthesis1.children().length() > 0) ? String(processingXML.paranthesis1) :  String(getDefaultValue("String"));
			var1= (processingXML.var1.children().length() > 0) ? String(processingXML.var1) :  String(getDefaultValue("String"));
			var1subscript= (processingXML.var1subscript.children().length() > 0) ? String(processingXML.var1subscript) :  String(getDefaultValue("String"));
			var1indice= (processingXML.var1indice.children().length() > 0) ? String(processingXML.var1indice) :  String(getDefaultValue("String"));
			postVariable1= (processingXML.postVariable1.children().length() > 0) ? String(processingXML.postVariable1) :  String(getDefaultValue("String"));
			operator1= (processingXML.operator1.children().length() > 0) ? String(processingXML.operator1) :  String(getDefaultValue("String"));
			var2= (processingXML.var2.children().length() > 0) ? String(processingXML.var2) :  String(getDefaultValue("String"));
			var2subscript= (processingXML.var2subscript.children().length() > 0) ? String(processingXML.var2subscript) :  String(getDefaultValue("String"));
			var2indice= (processingXML.var2indice.children().length() > 0) ? String(processingXML.var2indice) :  String(getDefaultValue("String"));
			postVariable2= (processingXML.postVariable2.children().length() > 0) ? String(processingXML.postVariable2) :  String(getDefaultValue("String"));
			operator2= (processingXML.operator2.children().length() > 0) ? String(processingXML.operator2) :  String(getDefaultValue("String"));
			var3= (processingXML.var3.children().length() > 0) ? String(processingXML.var3) :  String(getDefaultValue("String"));
			var3subscript= (processingXML.var3subscript.children().length() > 0) ? String(processingXML.var3subscript) :  String(getDefaultValue("String"));
			var3indice= (processingXML.var3indice.children().length() > 0) ? String(processingXML.var3indice) :  String(getDefaultValue("String"));
			postVariable3= (processingXML.postVariable3.children().length() > 0) ? String(processingXML.postVariable3) :  String(getDefaultValue("String"));
			paranthesis2= (processingXML.paranthesis2.children().length() > 0) ? String(processingXML.paranthesis2) :  String(getDefaultValue("String"));
			paranthesisindice= (processingXML.paranthesisindice.children().length() > 0) ? String(processingXML.paranthesisindice) :  String(getDefaultValue("String"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			rotationAngle= (processingXML.rotationAngle.children().length() > 0) ? int(processingXML.rotationAngle)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var cov_polynomial_3_inst_15:cov_polynomial_3 = lineToPlaceElement.Add_cov_polynomial_3(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,paranthesis1,var1,var1subscript,var1indice,postVariable1,operator1,var2,var2subscript,var2indice,postVariable2,operator2,var3,var3subscript,var3indice,postVariable3,paranthesis2,paranthesisindice,styleColor,hilightColor,rotationAngle,controlHeight);
			cov_polynomial_3_inst_15.alpha = 0;
			stageobj.addChild(cov_polynomial_3_inst_15);
			MovieClip(cov_polynomial_3_inst_15).addEventListener(Event.ENTER_FRAME, cov_polynomial_3_inst_15_AppearEvent);
			function cov_polynomial_3_inst_15_AppearEvent(event:Event):void {
				if (cov_polynomial_3_inst_15.alpha >= 1){
					cov_polynomial_3_inst_15.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, cov_polynomial_3_inst_15_AppearEvent);
				}else if(cov_polynomial_3_inst_15.alpha  < 0.5 ){
					cov_polynomial_3_inst_15.alpha = cov_polynomial_3_inst_15.alpha + 0.40;
				}else{
					cov_polynomial_3_inst_15.alpha = cov_polynomial_3_inst_15.alpha + 0.35;
				}
			}
			return cov_polynomial_3_inst_15;

		}



		public function Add_cov_polynomial_4():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,paranthesis1:String,var1:String,var1subscript:String,var1indice:String,postVariable1:String,operator1:String,var2:String,var2subscript:String,var2indice:String,postVariable2:String,operator2:String,var3:String,var3subscript:String,var3indice:String,postVariable3:String,operator3:String,var4:String,var4subscript:String,var4indice:String,postVariable4:String,paranthesis2:String,paranthesisindice:String,styleColor:String,hilightColor:String,rotationAngle:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			paranthesis1= (processingXML.paranthesis1.children().length() > 0) ? String(processingXML.paranthesis1) :  String(getDefaultValue("String"));
			var1= (processingXML.var1.children().length() > 0) ? String(processingXML.var1) :  String(getDefaultValue("String"));
			var1subscript= (processingXML.var1subscript.children().length() > 0) ? String(processingXML.var1subscript) :  String(getDefaultValue("String"));
			var1indice= (processingXML.var1indice.children().length() > 0) ? String(processingXML.var1indice) :  String(getDefaultValue("String"));
			postVariable1= (processingXML.postVariable1.children().length() > 0) ? String(processingXML.postVariable1) :  String(getDefaultValue("String"));
			operator1= (processingXML.operator1.children().length() > 0) ? String(processingXML.operator1) :  String(getDefaultValue("String"));
			var2= (processingXML.var2.children().length() > 0) ? String(processingXML.var2) :  String(getDefaultValue("String"));
			var2subscript= (processingXML.var2subscript.children().length() > 0) ? String(processingXML.var2subscript) :  String(getDefaultValue("String"));
			var2indice= (processingXML.var2indice.children().length() > 0) ? String(processingXML.var2indice) :  String(getDefaultValue("String"));
			postVariable2= (processingXML.postVariable2.children().length() > 0) ? String(processingXML.postVariable2) :  String(getDefaultValue("String"));
			operator2= (processingXML.operator2.children().length() > 0) ? String(processingXML.operator2) :  String(getDefaultValue("String"));
			var3= (processingXML.var3.children().length() > 0) ? String(processingXML.var3) :  String(getDefaultValue("String"));
			var3subscript= (processingXML.var3subscript.children().length() > 0) ? String(processingXML.var3subscript) :  String(getDefaultValue("String"));
			var3indice= (processingXML.var3indice.children().length() > 0) ? String(processingXML.var3indice) :  String(getDefaultValue("String"));
			postVariable3= (processingXML.postVariable3.children().length() > 0) ? String(processingXML.postVariable3) :  String(getDefaultValue("String"));
			operator3= (processingXML.operator3.children().length() > 0) ? String(processingXML.operator3) :  String(getDefaultValue("String"));
			var4= (processingXML.var4.children().length() > 0) ? String(processingXML.var4) :  String(getDefaultValue("String"));
			var4subscript= (processingXML.var4subscript.children().length() > 0) ? String(processingXML.var4subscript) :  String(getDefaultValue("String"));
			var4indice= (processingXML.var4indice.children().length() > 0) ? String(processingXML.var4indice) :  String(getDefaultValue("String"));
			postVariable4= (processingXML.postVariable4.children().length() > 0) ? String(processingXML.postVariable4) :  String(getDefaultValue("String"));
			paranthesis2= (processingXML.paranthesis2.children().length() > 0) ? String(processingXML.paranthesis2) :  String(getDefaultValue("String"));
			paranthesisindice= (processingXML.paranthesisindice.children().length() > 0) ? String(processingXML.paranthesisindice) :  String(getDefaultValue("String"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			rotationAngle= (processingXML.rotationAngle.children().length() > 0) ? int(processingXML.rotationAngle)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var cov_polynomial_4_inst_16:cov_polynomial_4 = lineToPlaceElement.Add_cov_polynomial_4(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,paranthesis1,var1,var1subscript,var1indice,postVariable1,operator1,var2,var2subscript,var2indice,postVariable2,operator2,var3,var3subscript,var3indice,postVariable3,operator3,var4,var4subscript,var4indice,postVariable4,paranthesis2,paranthesisindice,styleColor,hilightColor,rotationAngle,controlHeight);
			cov_polynomial_4_inst_16.alpha = 0;
			stageobj.addChild(cov_polynomial_4_inst_16);
			MovieClip(cov_polynomial_4_inst_16).addEventListener(Event.ENTER_FRAME, cov_polynomial_4_inst_16_AppearEvent);
			function cov_polynomial_4_inst_16_AppearEvent(event:Event):void {
				if (cov_polynomial_4_inst_16.alpha >= 1){
					cov_polynomial_4_inst_16.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, cov_polynomial_4_inst_16_AppearEvent);
				}else if(cov_polynomial_4_inst_16.alpha  < 0.5 ){
					cov_polynomial_4_inst_16.alpha = cov_polynomial_4_inst_16.alpha + 0.40;
				}else{
					cov_polynomial_4_inst_16.alpha = cov_polynomial_4_inst_16.alpha + 0.35;
				}
			}
			return cov_polynomial_4_inst_16;

		}



		public function Add_plainLine():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,x1:int,y1:int,coordinateType:String,x2:int,y2:int,angle:int,radius:int,p1ender:String,p1enderCoordinates:String,p2ender:String,p2enderCoordinates:String,showP1Coordinate:String,showP2Coordinate:String,styleColor:String,hilightColor:String,rotationAngle:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			x1= (processingXML.x1.children().length() > 0) ? int(processingXML.x1) :  int(getDefaultValue("int"));
			y1= (processingXML.y1.children().length() > 0) ? int(processingXML.y1) :  int(getDefaultValue("int"));
			coordinateType= (processingXML.coordinateType.children().length() > 0) ? String(processingXML.coordinateType) :  String(getDefaultValue("String"));
			x2= (processingXML.x2.children().length() > 0) ? int(processingXML.x2) :  int(getDefaultValue("int"));
			y2= (processingXML.y2.children().length() > 0) ? int(processingXML.y2) :  int(getDefaultValue("int"));
			angle= (processingXML.angle.children().length() > 0) ? int(processingXML.angle) :  int(getDefaultValue("int"));
			radius= (processingXML.radius.children().length() > 0) ? int(processingXML.radius) :  int(getDefaultValue("int"));
			p1ender= (processingXML.p1ender.children().length() > 0) ? String(processingXML.p1ender) :  String(getDefaultValue("String"));
			p1enderCoordinates= (processingXML.p1enderCoordinates.children().length() > 0) ? String(processingXML.p1enderCoordinates) :  String(getDefaultValue("String"));
			p2ender= (processingXML.p2ender.children().length() > 0) ? String(processingXML.p2ender) :  String(getDefaultValue("String"));
			p2enderCoordinates= (processingXML.p2enderCoordinates.children().length() > 0) ? String(processingXML.p2enderCoordinates) :  String(getDefaultValue("String"));
			showP1Coordinate= (processingXML.showP1Coordinate.children().length() > 0) ? String(processingXML.showP1Coordinate) :  String(getDefaultValue("String"));
			showP2Coordinate= (processingXML.showP2Coordinate.children().length() > 0) ? String(processingXML.showP2Coordinate) :  String(getDefaultValue("String"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			rotationAngle= (processingXML.rotationAngle.children().length() > 0) ? int(processingXML.rotationAngle)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var plainLine_inst_17:plainLine = lineToPlaceElement.Add_plainLine(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,x1,y1,coordinateType,x2,y2,angle,radius,p1ender,p1enderCoordinates,p2ender,p2enderCoordinates,showP1Coordinate,showP2Coordinate,styleColor,hilightColor,rotationAngle,controlHeight);
			plainLine_inst_17.alpha = 0;
			stageobj.addChild(plainLine_inst_17);
			MovieClip(plainLine_inst_17).addEventListener(Event.ENTER_FRAME, plainLine_inst_17_AppearEvent);
			function plainLine_inst_17_AppearEvent(event:Event):void {
				if (plainLine_inst_17.alpha >= 1){
					plainLine_inst_17.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, plainLine_inst_17_AppearEvent);
				}else if(plainLine_inst_17.alpha  < 0.5 ){
					plainLine_inst_17.alpha = plainLine_inst_17.alpha + 0.40;
				}else{
					plainLine_inst_17.alpha = plainLine_inst_17.alpha + 0.35;
				}
			}
			return plainLine_inst_17;

		}



		public function Add_plainCircle():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,x1:int,y1:int,radius:int,ringRadius:int,styleColor:String,fillCircle:Boolean,fillColor:String,hilightColor:String,transparancyPercentage:String,rotationAngle:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			x1= (processingXML.x1.children().length() > 0) ? int(processingXML.x1) :  int(getDefaultValue("int"));
			y1= (processingXML.y1.children().length() > 0) ? int(processingXML.y1) :  int(getDefaultValue("int"));
			radius= (processingXML.radius.children().length() > 0) ? int(processingXML.radius) :  int(getDefaultValue("int"));
			ringRadius= (processingXML.ringRadius.children().length() > 0) ? int(processingXML.ringRadius) :  int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			fillCircle=  (processingXML.fillCircle.children().length() > 0)  ?  getBooleanValue(processingXML.fillCircle) : getBooleanValue("false");
			fillColor= (processingXML.fillColor.children().length() > 0) ? String(processingXML.fillColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			transparancyPercentage= (processingXML.transparancyPercentage.children().length() > 0) ? String(processingXML.transparancyPercentage)  :  String(getDefaultValue("String"));
			rotationAngle= (processingXML.rotationAngle.children().length() > 0) ? int(processingXML.rotationAngle)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var plainCircle_inst_18:plainCircle = lineToPlaceElement.Add_plainCircle(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,x1,y1,radius,ringRadius,styleColor,fillCircle,fillColor,hilightColor,transparancyPercentage,rotationAngle,controlHeight);
			plainCircle_inst_18.alpha = 0;
			stageobj.addChild(plainCircle_inst_18);
			MovieClip(plainCircle_inst_18).addEventListener(Event.ENTER_FRAME, plainCircle_inst_18_AppearEvent);
			function plainCircle_inst_18_AppearEvent(event:Event):void {
				if (plainCircle_inst_18.alpha >= 1){
					plainCircle_inst_18.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, plainCircle_inst_18_AppearEvent);
				}else if(plainCircle_inst_18.alpha  < 0.5 ){
					plainCircle_inst_18.alpha = plainCircle_inst_18.alpha + 0.40;
				}else{
					plainCircle_inst_18.alpha = plainCircle_inst_18.alpha + 0.35;
				}
			}
			return plainCircle_inst_18;

		}



		public function Add_plainSquare():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,x1:int,y1:int,length:int,height:int,styleColor:String,fillSquare:Boolean,fillColor:String,hilightColor:String,transparancyPercentage:String,rotationAngle:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			x1= (processingXML.x1.children().length() > 0) ? int(processingXML.x1) :  int(getDefaultValue("int"));
			y1= (processingXML.y1.children().length() > 0) ? int(processingXML.y1) :  int(getDefaultValue("int"));
			length= (processingXML.length.children().length() > 0) ? int(processingXML.length) :  int(getDefaultValue("int"));
			height= (processingXML.height.children().length() > 0) ? int(processingXML.height) :  int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			fillSquare=  (processingXML.fillSquare.children().length() > 0)  ?  getBooleanValue(processingXML.fillSquare) : getBooleanValue("false");
			fillColor= (processingXML.fillColor.children().length() > 0) ? String(processingXML.fillColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			transparancyPercentage= (processingXML.transparancyPercentage.children().length() > 0) ? String(processingXML.transparancyPercentage)  :  String(getDefaultValue("String"));
			rotationAngle= (processingXML.rotationAngle.children().length() > 0) ? int(processingXML.rotationAngle)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var plainSquare_inst_19:plainSquare = lineToPlaceElement.Add_plainSquare(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,x1,y1,length,height,styleColor,fillSquare,fillColor,hilightColor,transparancyPercentage,rotationAngle,controlHeight);
			plainSquare_inst_19.alpha = 0;
			stageobj.addChild(plainSquare_inst_19);
			MovieClip(plainSquare_inst_19).addEventListener(Event.ENTER_FRAME, plainSquare_inst_19_AppearEvent);
			function plainSquare_inst_19_AppearEvent(event:Event):void {
				if (plainSquare_inst_19.alpha >= 1){
					plainSquare_inst_19.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, plainSquare_inst_19_AppearEvent);
				}else if(plainSquare_inst_19.alpha  < 0.5 ){
					plainSquare_inst_19.alpha = plainSquare_inst_19.alpha + 0.40;
				}else{
					plainSquare_inst_19.alpha = plainSquare_inst_19.alpha + 0.35;
				}
			}
			return plainSquare_inst_19;

		}



		public function Add_squareBlocks():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,squareBlocksSize:String,squareBlocksDimension:String,cellColors:String,styleColor:String,hilightColor:String,rotationAngle:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			squareBlocksSize= (processingXML.squareBlocksSize.children().length() > 0) ? String(processingXML.squareBlocksSize) :  String(getDefaultValue("String"));
			squareBlocksDimension= (processingXML.squareBlocksDimension.children().length() > 0) ? String(processingXML.squareBlocksDimension) :  String(getDefaultValue("String"));
			cellColors= (processingXML.cellColors.children().length() > 0) ? String(processingXML.cellColors) :  String(getDefaultValue("String"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			rotationAngle= (processingXML.rotationAngle.children().length() > 0) ? int(processingXML.rotationAngle)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var squareBlocks_inst_20:squareBlocks = lineToPlaceElement.Add_squareBlocks(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,squareBlocksSize,squareBlocksDimension,cellColors,styleColor,hilightColor,rotationAngle,controlHeight);
			squareBlocks_inst_20.alpha = 0;
			stageobj.addChild(squareBlocks_inst_20);
			MovieClip(squareBlocks_inst_20).addEventListener(Event.ENTER_FRAME, squareBlocks_inst_20_AppearEvent);
			function squareBlocks_inst_20_AppearEvent(event:Event):void {
				if (squareBlocks_inst_20.alpha >= 1){
					squareBlocks_inst_20.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, squareBlocks_inst_20_AppearEvent);
				}else if(squareBlocks_inst_20.alpha  < 0.5 ){
					squareBlocks_inst_20.alpha = squareBlocks_inst_20.alpha + 0.40;
				}else{
					squareBlocks_inst_20.alpha = squareBlocks_inst_20.alpha + 0.35;
				}
			}
			return squareBlocks_inst_20;

		}



		public function Add_plainTriangle():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,p1x:int,p1y:int,p2CoordinateType:String,p2x:int,p2y:int,p2angle:int,p2radius:int,p3CoordinateType:String,p3x:int,p3y:int,p3angle:int,p3radius:int,fillTriangle:Boolean,styleColor:String,fillColor:String,hilightColor:String,transparancyPercentage:String,rotationAngle:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			p1x= (processingXML.p1x.children().length() > 0) ? int(processingXML.p1x) :  int(getDefaultValue("int"));
			p1y= (processingXML.p1y.children().length() > 0) ? int(processingXML.p1y) :  int(getDefaultValue("int"));
			p2CoordinateType= (processingXML.p2CoordinateType.children().length() > 0) ? String(processingXML.p2CoordinateType) :  String(getDefaultValue("String"));
			p2x= (processingXML.p2x.children().length() > 0) ? int(processingXML.p2x) :  int(getDefaultValue("int"));
			p2y= (processingXML.p2y.children().length() > 0) ? int(processingXML.p2y) :  int(getDefaultValue("int"));
			p2angle= (processingXML.p2angle.children().length() > 0) ? int(processingXML.p2angle) :  int(getDefaultValue("int"));
			p2radius= (processingXML.p2radius.children().length() > 0) ? int(processingXML.p2radius) :  int(getDefaultValue("int"));
			p3CoordinateType= (processingXML.p3CoordinateType.children().length() > 0) ? String(processingXML.p3CoordinateType) :  String(getDefaultValue("String"));
			p3x= (processingXML.p3x.children().length() > 0) ? int(processingXML.p3x) :  int(getDefaultValue("int"));
			p3y= (processingXML.p3y.children().length() > 0) ? int(processingXML.p3y) :  int(getDefaultValue("int"));
			p3angle= (processingXML.p3angle.children().length() > 0) ? int(processingXML.p3angle) :  int(getDefaultValue("int"));
			p3radius= (processingXML.p3radius.children().length() > 0) ? int(processingXML.p3radius) :  int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			fillTriangle=  (processingXML.fillTriangle.children().length() > 0)  ?  getBooleanValue(processingXML.fillTriangle) : getBooleanValue("false");
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			fillColor= (processingXML.fillColor.children().length() > 0) ? String(processingXML.fillColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			transparancyPercentage= (processingXML.transparancyPercentage.children().length() > 0) ? String(processingXML.transparancyPercentage)  :  String(getDefaultValue("String"));
			rotationAngle= (processingXML.rotationAngle.children().length() > 0) ? int(processingXML.rotationAngle)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var plainTriangle_inst_21:plainTriangle = lineToPlaceElement.Add_plainTriangle(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,p1x,p1y,p2CoordinateType,p2x,p2y,p2angle,p2radius,p3CoordinateType,p3x,p3y,p3angle,p3radius,fillTriangle,styleColor,fillColor,hilightColor,transparancyPercentage,rotationAngle,controlHeight);
			plainTriangle_inst_21.alpha = 0;
			stageobj.addChild(plainTriangle_inst_21);
			MovieClip(plainTriangle_inst_21).addEventListener(Event.ENTER_FRAME, plainTriangle_inst_21_AppearEvent);
			function plainTriangle_inst_21_AppearEvent(event:Event):void {
				if (plainTriangle_inst_21.alpha >= 1){
					plainTriangle_inst_21.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, plainTriangle_inst_21_AppearEvent);
				}else if(plainTriangle_inst_21.alpha  < 0.5 ){
					plainTriangle_inst_21.alpha = plainTriangle_inst_21.alpha + 0.40;
				}else{
					plainTriangle_inst_21.alpha = plainTriangle_inst_21.alpha + 0.35;
				}
			}
			return plainTriangle_inst_21;

		}



		public function Add_plainEllipse():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,topLeftX:int,topLeftY:int,width:int,height:int,fillEllipse:Boolean,styleColor:String,fillColor:String,hilightColor:String,transparancyPercentage:String,rotationAngle:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			topLeftX= (processingXML.topLeftX.children().length() > 0) ? int(processingXML.topLeftX) :  int(getDefaultValue("int"));
			topLeftY= (processingXML.topLeftY.children().length() > 0) ? int(processingXML.topLeftY) :  int(getDefaultValue("int"));
			width= (processingXML.width.children().length() > 0) ? int(processingXML.width) :  int(getDefaultValue("int"));
			height= (processingXML.height.children().length() > 0) ? int(processingXML.height) :  int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			fillEllipse=  (processingXML.fillEllipse.children().length() > 0)  ?  getBooleanValue(processingXML.fillEllipse) : getBooleanValue("false");
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			fillColor= (processingXML.fillColor.children().length() > 0) ? String(processingXML.fillColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			transparancyPercentage= (processingXML.transparancyPercentage.children().length() > 0) ? String(processingXML.transparancyPercentage)  :  String(getDefaultValue("String"));
			rotationAngle= (processingXML.rotationAngle.children().length() > 0) ? int(processingXML.rotationAngle)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var plainEllipse_inst_22:plainEllipse = lineToPlaceElement.Add_plainEllipse(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,topLeftX,topLeftY,width,height,fillEllipse,styleColor,fillColor,hilightColor,transparancyPercentage,rotationAngle,controlHeight);
			plainEllipse_inst_22.alpha = 0;
			stageobj.addChild(plainEllipse_inst_22);
			MovieClip(plainEllipse_inst_22).addEventListener(Event.ENTER_FRAME, plainEllipse_inst_22_AppearEvent);
			function plainEllipse_inst_22_AppearEvent(event:Event):void {
				if (plainEllipse_inst_22.alpha >= 1){
					plainEllipse_inst_22.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, plainEllipse_inst_22_AppearEvent);
				}else if(plainEllipse_inst_22.alpha  < 0.5 ){
					plainEllipse_inst_22.alpha = plainEllipse_inst_22.alpha + 0.40;
				}else{
					plainEllipse_inst_22.alpha = plainEllipse_inst_22.alpha + 0.35;
				}
			}
			return plainEllipse_inst_22;

		}



		public function Add_plainPloygon15():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,p1:String,p2:String,p3:String,p4:String,p5:String,p6:String,p7:String,p8:String,p9:String,p10:String,p11:String,p12:String,p13:String,p14:String,p15:String,fillPloygon:Boolean,styleColor:String,fillColor:String,hilightColor:String,transparancyPercentage:String,lineThickness:int,showPointPugs:Boolean,pugSize:int,pugColor:String,rotationAngle:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			p1= (processingXML.p1.children().length() > 0) ? String(processingXML.p1) :  String(getDefaultValue("String"));
			p2= (processingXML.p2.children().length() > 0) ? String(processingXML.p2) :  String(getDefaultValue("String"));
			p3= (processingXML.p3.children().length() > 0) ? String(processingXML.p3) :  String(getDefaultValue("String"));
			p4= (processingXML.p4.children().length() > 0) ? String(processingXML.p4) :  String(getDefaultValue("String"));
			p5= (processingXML.p5.children().length() > 0) ? String(processingXML.p5) :  String(getDefaultValue("String"));
			p6= (processingXML.p6.children().length() > 0) ? String(processingXML.p6) :  String(getDefaultValue("String"));
			p7= (processingXML.p7.children().length() > 0) ? String(processingXML.p7) :  String(getDefaultValue("String"));
			p8= (processingXML.p8.children().length() > 0) ? String(processingXML.p8) :  String(getDefaultValue("String"));
			p9= (processingXML.p9.children().length() > 0) ? String(processingXML.p9) :  String(getDefaultValue("String"));
			p10= (processingXML.p10.children().length() > 0) ? String(processingXML.p10) :  String(getDefaultValue("String"));
			p11= (processingXML.p11.children().length() > 0) ? String(processingXML.p11) :  String(getDefaultValue("String"));
			p12= (processingXML.p12.children().length() > 0) ? String(processingXML.p12) :  String(getDefaultValue("String"));
			p13= (processingXML.p13.children().length() > 0) ? String(processingXML.p13) :  String(getDefaultValue("String"));
			p14= (processingXML.p14.children().length() > 0) ? String(processingXML.p14) :  String(getDefaultValue("String"));
			p15= (processingXML.p15.children().length() > 0) ? String(processingXML.p15) :  String(getDefaultValue("String"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			fillPloygon=  (processingXML.fillPloygon.children().length() > 0)  ?  getBooleanValue(processingXML.fillPloygon) : getBooleanValue("false");
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			fillColor= (processingXML.fillColor.children().length() > 0) ? String(processingXML.fillColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			transparancyPercentage= (processingXML.transparancyPercentage.children().length() > 0) ? String(processingXML.transparancyPercentage)  :  String(getDefaultValue("String"));
			lineThickness= (processingXML.lineThickness.children().length() > 0) ? int(processingXML.lineThickness)  :  int(getDefaultValue("int"));
			showPointPugs=  (processingXML.showPointPugs.children().length() > 0)  ?  getBooleanValue(processingXML.showPointPugs) : getBooleanValue("false");
			pugSize= (processingXML.pugSize.children().length() > 0) ? int(processingXML.pugSize)  :  int(getDefaultValue("int"));
			pugColor= (processingXML.pugColor.children().length() > 0) ? String(processingXML.pugColor)  :  String(getDefaultValue("String"));
			rotationAngle= (processingXML.rotationAngle.children().length() > 0) ? int(processingXML.rotationAngle)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var plainPloygon15_inst_23:plainPloygon15 = lineToPlaceElement.Add_plainPloygon15(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,fillPloygon,styleColor,fillColor,hilightColor,transparancyPercentage,lineThickness,showPointPugs,pugSize,pugColor,rotationAngle,controlHeight);
			plainPloygon15_inst_23.alpha = 0;
			stageobj.addChild(plainPloygon15_inst_23);
			MovieClip(plainPloygon15_inst_23).addEventListener(Event.ENTER_FRAME, plainPloygon15_inst_23_AppearEvent);
			function plainPloygon15_inst_23_AppearEvent(event:Event):void {
				if (plainPloygon15_inst_23.alpha >= 1){
					plainPloygon15_inst_23.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, plainPloygon15_inst_23_AppearEvent);
				}else if(plainPloygon15_inst_23.alpha  < 0.5 ){
					plainPloygon15_inst_23.alpha = plainPloygon15_inst_23.alpha + 0.40;
				}else{
					plainPloygon15_inst_23.alpha = plainPloygon15_inst_23.alpha + 0.35;
				}
			}
			return plainPloygon15_inst_23;

		}



		public function Add_plainArc():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,centerX:int,centerY:int,radius:int,startingAngle:int,arcAngle:int,startingArrow:String,endingArrow:String,styleColor:String,hilightColor:String,rotationAngle:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			centerX= (processingXML.centerX.children().length() > 0) ? int(processingXML.centerX) :  int(getDefaultValue("int"));
			centerY= (processingXML.centerY.children().length() > 0) ? int(processingXML.centerY) :  int(getDefaultValue("int"));
			radius= (processingXML.radius.children().length() > 0) ? int(processingXML.radius) :  int(getDefaultValue("int"));
			startingAngle= (processingXML.startingAngle.children().length() > 0) ? int(processingXML.startingAngle) :  int(getDefaultValue("int"));
			arcAngle= (processingXML.arcAngle.children().length() > 0) ? int(processingXML.arcAngle) :  int(getDefaultValue("int"));
			startingArrow= (processingXML.startingArrow.children().length() > 0) ? String(processingXML.startingArrow) :  String(getDefaultValue("String"));
			endingArrow= (processingXML.endingArrow.children().length() > 0) ? String(processingXML.endingArrow) :  String(getDefaultValue("String"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			rotationAngle= (processingXML.rotationAngle.children().length() > 0) ? int(processingXML.rotationAngle)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var plainArc_inst_24:plainArc = lineToPlaceElement.Add_plainArc(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,centerX,centerY,radius,startingAngle,arcAngle,startingArrow,endingArrow,styleColor,hilightColor,rotationAngle,controlHeight);
			plainArc_inst_24.alpha = 0;
			stageobj.addChild(plainArc_inst_24);
			MovieClip(plainArc_inst_24).addEventListener(Event.ENTER_FRAME, plainArc_inst_24_AppearEvent);
			function plainArc_inst_24_AppearEvent(event:Event):void {
				if (plainArc_inst_24.alpha >= 1){
					plainArc_inst_24.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, plainArc_inst_24_AppearEvent);
				}else if(plainArc_inst_24.alpha  < 0.5 ){
					plainArc_inst_24.alpha = plainArc_inst_24.alpha + 0.40;
				}else{
					plainArc_inst_24.alpha = plainArc_inst_24.alpha + 0.35;
				}
			}
			return plainArc_inst_24;

		}



		public function Add_segmentedCircle():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,centerX:int,centerY:int,outerRadius:int,innerRadius:int,segmentStartAngle:int,segmentAngles:String,segmentFill:String,styleColor:String,fillColor:String,hilightColor:String,transparancyPercentage:String,rotationAngle:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			centerX= (processingXML.centerX.children().length() > 0) ? int(processingXML.centerX) :  int(getDefaultValue("int"));
			centerY= (processingXML.centerY.children().length() > 0) ? int(processingXML.centerY) :  int(getDefaultValue("int"));
			outerRadius= (processingXML.outerRadius.children().length() > 0) ? int(processingXML.outerRadius) :  int(getDefaultValue("int"));
			innerRadius= (processingXML.innerRadius.children().length() > 0) ? int(processingXML.innerRadius) :  int(getDefaultValue("int"));
			segmentStartAngle= (processingXML.segmentStartAngle.children().length() > 0) ? int(processingXML.segmentStartAngle) :  int(getDefaultValue("int"));
			segmentAngles= (processingXML.segmentAngles.children().length() > 0) ? String(processingXML.segmentAngles) :  String(getDefaultValue("String"));
			segmentFill= (processingXML.segmentFill.children().length() > 0) ? String(processingXML.segmentFill) :  String(getDefaultValue("String"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			fillColor= (processingXML.fillColor.children().length() > 0) ? String(processingXML.fillColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			transparancyPercentage= (processingXML.transparancyPercentage.children().length() > 0) ? String(processingXML.transparancyPercentage)  :  String(getDefaultValue("String"));
			rotationAngle= (processingXML.rotationAngle.children().length() > 0) ? int(processingXML.rotationAngle)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var segmentedCircle_inst_25:segmentedCircle = lineToPlaceElement.Add_segmentedCircle(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,centerX,centerY,outerRadius,innerRadius,segmentStartAngle,segmentAngles,segmentFill,styleColor,fillColor,hilightColor,transparancyPercentage,rotationAngle,controlHeight);
			segmentedCircle_inst_25.alpha = 0;
			stageobj.addChild(segmentedCircle_inst_25);
			MovieClip(segmentedCircle_inst_25).addEventListener(Event.ENTER_FRAME, segmentedCircle_inst_25_AppearEvent);
			function segmentedCircle_inst_25_AppearEvent(event:Event):void {
				if (segmentedCircle_inst_25.alpha >= 1){
					segmentedCircle_inst_25.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, segmentedCircle_inst_25_AppearEvent);
				}else if(segmentedCircle_inst_25.alpha  < 0.5 ){
					segmentedCircle_inst_25.alpha = segmentedCircle_inst_25.alpha + 0.40;
				}else{
					segmentedCircle_inst_25.alpha = segmentedCircle_inst_25.alpha + 0.35;
				}
			}
			return segmentedCircle_inst_25;

		}



		public function Add_segmentedSquare():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,x1:int,y1:int,length:int,height:int,segmentStyle:String,segmentFill:String,styleColor:String,segmentFillColor:String,hilightColor:String,transparancyPercentage:String,rotationAngle:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			x1= (processingXML.x1.children().length() > 0) ? int(processingXML.x1) :  int(getDefaultValue("int"));
			y1= (processingXML.y1.children().length() > 0) ? int(processingXML.y1) :  int(getDefaultValue("int"));
			length= (processingXML.length.children().length() > 0) ? int(processingXML.length) :  int(getDefaultValue("int"));
			height= (processingXML.height.children().length() > 0) ? int(processingXML.height) :  int(getDefaultValue("int"));
			segmentStyle= (processingXML.segmentStyle.children().length() > 0) ? String(processingXML.segmentStyle) :  String(getDefaultValue("String"));
			segmentFill= (processingXML.segmentFill.children().length() > 0) ? String(processingXML.segmentFill) :  String(getDefaultValue("String"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			segmentFillColor= (processingXML.segmentFillColor.children().length() > 0) ? String(processingXML.segmentFillColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			transparancyPercentage= (processingXML.transparancyPercentage.children().length() > 0) ? String(processingXML.transparancyPercentage)  :  String(getDefaultValue("String"));
			rotationAngle= (processingXML.rotationAngle.children().length() > 0) ? int(processingXML.rotationAngle)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var segmentedSquare_inst_26:segmentedSquare = lineToPlaceElement.Add_segmentedSquare(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,x1,y1,length,height,segmentStyle,segmentFill,styleColor,segmentFillColor,hilightColor,transparancyPercentage,rotationAngle,controlHeight);
			segmentedSquare_inst_26.alpha = 0;
			stageobj.addChild(segmentedSquare_inst_26);
			MovieClip(segmentedSquare_inst_26).addEventListener(Event.ENTER_FRAME, segmentedSquare_inst_26_AppearEvent);
			function segmentedSquare_inst_26_AppearEvent(event:Event):void {
				if (segmentedSquare_inst_26.alpha >= 1){
					segmentedSquare_inst_26.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, segmentedSquare_inst_26_AppearEvent);
				}else if(segmentedSquare_inst_26.alpha  < 0.5 ){
					segmentedSquare_inst_26.alpha = segmentedSquare_inst_26.alpha + 0.40;
				}else{
					segmentedSquare_inst_26.alpha = segmentedSquare_inst_26.alpha + 0.35;
				}
			}
			return segmentedSquare_inst_26;

		}



		public function Add_graphBaseSheet():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,x1:int,y1:int,length:int,height:int,minXAxis:int,maxXAxis:int,minYAxis:int,maxYAxis:int,showXAxisLines:Boolean,xAxisLineInterval:int,showYAxisLines:Boolean,yAxisLineInterval:int,xAxisLableDisplayInterval:int,xAxisLableDisplayPosition:String,yAxisLableDisplayInterval:int,yAxisLableDisplayPosition:String,lineColor:String,axisColor:String,backgroundColor:String,borderColor:String,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			x1= (processingXML.x1.children().length() > 0) ? int(processingXML.x1) :  int(getDefaultValue("int"));
			y1= (processingXML.y1.children().length() > 0) ? int(processingXML.y1) :  int(getDefaultValue("int"));
			length= (processingXML.length.children().length() > 0) ? int(processingXML.length) :  int(getDefaultValue("int"));
			height= (processingXML.height.children().length() > 0) ? int(processingXML.height) :  int(getDefaultValue("int"));
			minXAxis= (processingXML.minXAxis.children().length() > 0) ? int(processingXML.minXAxis) :  int(getDefaultValue("int"));
			maxXAxis= (processingXML.maxXAxis.children().length() > 0) ? int(processingXML.maxXAxis) :  int(getDefaultValue("int"));
			minYAxis= (processingXML.minYAxis.children().length() > 0) ? int(processingXML.minYAxis) :  int(getDefaultValue("int"));
			maxYAxis= (processingXML.maxYAxis.children().length() > 0) ? int(processingXML.maxYAxis) :  int(getDefaultValue("int"));
			showXAxisLines=  (processingXML.showXAxisLines.children().length() > 0)  ? getBooleanValue(processingXML.showXAxisLines) : getBooleanValue("false"); 
			xAxisLineInterval= (processingXML.xAxisLineInterval.children().length() > 0) ? int(processingXML.xAxisLineInterval) :  int(getDefaultValue("int"));
			showYAxisLines=  (processingXML.showYAxisLines.children().length() > 0)  ? getBooleanValue(processingXML.showYAxisLines) : getBooleanValue("false"); 
			yAxisLineInterval= (processingXML.yAxisLineInterval.children().length() > 0) ? int(processingXML.yAxisLineInterval) :  int(getDefaultValue("int"));
			xAxisLableDisplayInterval= (processingXML.xAxisLableDisplayInterval.children().length() > 0) ? int(processingXML.xAxisLableDisplayInterval) :  int(getDefaultValue("int"));
			xAxisLableDisplayPosition= (processingXML.xAxisLableDisplayPosition.children().length() > 0) ? String(processingXML.xAxisLableDisplayPosition) :  String(getDefaultValue("String"));
			yAxisLableDisplayInterval= (processingXML.yAxisLableDisplayInterval.children().length() > 0) ? int(processingXML.yAxisLableDisplayInterval) :  int(getDefaultValue("int"));
			yAxisLableDisplayPosition= (processingXML.yAxisLableDisplayPosition.children().length() > 0) ? String(processingXML.yAxisLableDisplayPosition) :  String(getDefaultValue("String"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			lineColor= (processingXML.lineColor.children().length() > 0) ? String(processingXML.lineColor)  :  String(getDefaultValue("String"));
			axisColor= (processingXML.axisColor.children().length() > 0) ? String(processingXML.axisColor)  :  String(getDefaultValue("String"));
			backgroundColor= (processingXML.backgroundColor.children().length() > 0) ? String(processingXML.backgroundColor)  :  String(getDefaultValue("String"));
			borderColor= (processingXML.borderColor.children().length() > 0) ? String(processingXML.borderColor)  :  String(getDefaultValue("String"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var graphBaseSheet_inst_27:graphBaseSheet = lineToPlaceElement.Add_graphBaseSheet(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,x1,y1,length,height,minXAxis,maxXAxis,minYAxis,maxYAxis,showXAxisLines,xAxisLineInterval,showYAxisLines,yAxisLineInterval,xAxisLableDisplayInterval,xAxisLableDisplayPosition,yAxisLableDisplayInterval,yAxisLableDisplayPosition,lineColor,axisColor,backgroundColor,borderColor,controlHeight);
			graphBaseSheet_inst_27.alpha = 0;
			stageobj.addChild(graphBaseSheet_inst_27);
			MovieClip(graphBaseSheet_inst_27).addEventListener(Event.ENTER_FRAME, graphBaseSheet_inst_27_AppearEvent);
			function graphBaseSheet_inst_27_AppearEvent(event:Event):void {
				if (graphBaseSheet_inst_27.alpha >= 1){
					graphBaseSheet_inst_27.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, graphBaseSheet_inst_27_AppearEvent);
				}else if(graphBaseSheet_inst_27.alpha  < 0.5 ){
					graphBaseSheet_inst_27.alpha = graphBaseSheet_inst_27.alpha + 0.40;
				}else{
					graphBaseSheet_inst_27.alpha = graphBaseSheet_inst_27.alpha + 0.35;
				}
			}
			return graphBaseSheet_inst_27;

		}



		public function Add_graphPlainLine():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,x1:int,y1:int,coordinateType:String,x2:int,y2:int,angle:int,radius:int,p1ender:String,p1enderCoordinates:String,p2ender:String,p2enderCoordinates:String,showP1Coordinate:String,showP2Coordinate:String,arrowLength:int,styleColor:String,hilightColor:String,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			x1= (processingXML.x1.children().length() > 0) ? int(processingXML.x1) :  int(getDefaultValue("int"));
			y1= (processingXML.y1.children().length() > 0) ? int(processingXML.y1) :  int(getDefaultValue("int"));
			coordinateType= (processingXML.coordinateType.children().length() > 0) ? String(processingXML.coordinateType) :  String(getDefaultValue("String"));
			x2= (processingXML.x2.children().length() > 0) ? int(processingXML.x2) :  int(getDefaultValue("int"));
			y2= (processingXML.y2.children().length() > 0) ? int(processingXML.y2) :  int(getDefaultValue("int"));
			angle= (processingXML.angle.children().length() > 0) ? int(processingXML.angle) :  int(getDefaultValue("int"));
			radius= (processingXML.radius.children().length() > 0) ? int(processingXML.radius) :  int(getDefaultValue("int"));
			p1ender= (processingXML.p1ender.children().length() > 0) ? String(processingXML.p1ender) :  String(getDefaultValue("String"));
			p1enderCoordinates= (processingXML.p1enderCoordinates.children().length() > 0) ? String(processingXML.p1enderCoordinates) :  String(getDefaultValue("String"));
			p2ender= (processingXML.p2ender.children().length() > 0) ? String(processingXML.p2ender) :  String(getDefaultValue("String"));
			p2enderCoordinates= (processingXML.p2enderCoordinates.children().length() > 0) ? String(processingXML.p2enderCoordinates) :  String(getDefaultValue("String"));
			showP1Coordinate= (processingXML.showP1Coordinate.children().length() > 0) ? String(processingXML.showP1Coordinate) :  String(getDefaultValue("String"));
			showP2Coordinate= (processingXML.showP2Coordinate.children().length() > 0) ? String(processingXML.showP2Coordinate) :  String(getDefaultValue("String"));
			arrowLength= (processingXML.arrowLength.children().length() > 0) ? int(processingXML.arrowLength) :  int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var graphPlainLine_inst_28:graphPlainLine = lineToPlaceElement.Add_graphPlainLine(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,x1,y1,coordinateType,x2,y2,angle,radius,p1ender,p1enderCoordinates,p2ender,p2enderCoordinates,showP1Coordinate,showP2Coordinate,arrowLength,styleColor,hilightColor,controlHeight);
			graphPlainLine_inst_28.alpha = 0;
			stageobj.addChild(graphPlainLine_inst_28);
			MovieClip(graphPlainLine_inst_28).addEventListener(Event.ENTER_FRAME, graphPlainLine_inst_28_AppearEvent);
			function graphPlainLine_inst_28_AppearEvent(event:Event):void {
				if (graphPlainLine_inst_28.alpha >= 1){
					graphPlainLine_inst_28.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, graphPlainLine_inst_28_AppearEvent);
				}else if(graphPlainLine_inst_28.alpha  < 0.5 ){
					graphPlainLine_inst_28.alpha = graphPlainLine_inst_28.alpha + 0.40;
				}else{
					graphPlainLine_inst_28.alpha = graphPlainLine_inst_28.alpha + 0.35;
				}
			}
			return graphPlainLine_inst_28;

		}



		public function Add_graphPlainSquare():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,x1:int,y1:int,length:int,height:int,styleColor:String,fillSquare:Boolean,fillColor:String,hilightColor:String,transparancyPercentage:String,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			x1= (processingXML.x1.children().length() > 0) ? int(processingXML.x1) :  int(getDefaultValue("int"));
			y1= (processingXML.y1.children().length() > 0) ? int(processingXML.y1) :  int(getDefaultValue("int"));
			length= (processingXML.length.children().length() > 0) ? int(processingXML.length) :  int(getDefaultValue("int"));
			height= (processingXML.height.children().length() > 0) ? int(processingXML.height) :  int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			fillSquare=  (processingXML.fillSquare.children().length() > 0)  ?  getBooleanValue(processingXML.fillSquare) : getBooleanValue("false");
			fillColor= (processingXML.fillColor.children().length() > 0) ? String(processingXML.fillColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			transparancyPercentage= (processingXML.transparancyPercentage.children().length() > 0) ? String(processingXML.transparancyPercentage)  :  String(getDefaultValue("String"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var graphPlainSquare_inst_29:graphPlainSquare = lineToPlaceElement.Add_graphPlainSquare(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,x1,y1,length,height,styleColor,fillSquare,fillColor,hilightColor,transparancyPercentage,controlHeight);
			graphPlainSquare_inst_29.alpha = 0;
			stageobj.addChild(graphPlainSquare_inst_29);
			MovieClip(graphPlainSquare_inst_29).addEventListener(Event.ENTER_FRAME, graphPlainSquare_inst_29_AppearEvent);
			function graphPlainSquare_inst_29_AppearEvent(event:Event):void {
				if (graphPlainSquare_inst_29.alpha >= 1){
					graphPlainSquare_inst_29.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, graphPlainSquare_inst_29_AppearEvent);
				}else if(graphPlainSquare_inst_29.alpha  < 0.5 ){
					graphPlainSquare_inst_29.alpha = graphPlainSquare_inst_29.alpha + 0.40;
				}else{
					graphPlainSquare_inst_29.alpha = graphPlainSquare_inst_29.alpha + 0.35;
				}
			}
			return graphPlainSquare_inst_29;

		}



		public function Add_graphPlainTriangle():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,p1x:int,p1y:int,p2CoordinateType:String,p2x:int,p2y:int,p2angle:int,p2radius:int,p3CoordinateType:String,p3x:int,p3y:int,p3angle:int,p3radius:int,fillTriangle:Boolean,styleColor:String,fillColor:String,hilightColor:String,transparancyPercentage:String,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			p1x= (processingXML.p1x.children().length() > 0) ? int(processingXML.p1x) :  int(getDefaultValue("int"));
			p1y= (processingXML.p1y.children().length() > 0) ? int(processingXML.p1y) :  int(getDefaultValue("int"));
			p2CoordinateType= (processingXML.p2CoordinateType.children().length() > 0) ? String(processingXML.p2CoordinateType) :  String(getDefaultValue("String"));
			p2x= (processingXML.p2x.children().length() > 0) ? int(processingXML.p2x) :  int(getDefaultValue("int"));
			p2y= (processingXML.p2y.children().length() > 0) ? int(processingXML.p2y) :  int(getDefaultValue("int"));
			p2angle= (processingXML.p2angle.children().length() > 0) ? int(processingXML.p2angle) :  int(getDefaultValue("int"));
			p2radius= (processingXML.p2radius.children().length() > 0) ? int(processingXML.p2radius) :  int(getDefaultValue("int"));
			p3CoordinateType= (processingXML.p3CoordinateType.children().length() > 0) ? String(processingXML.p3CoordinateType) :  String(getDefaultValue("String"));
			p3x= (processingXML.p3x.children().length() > 0) ? int(processingXML.p3x) :  int(getDefaultValue("int"));
			p3y= (processingXML.p3y.children().length() > 0) ? int(processingXML.p3y) :  int(getDefaultValue("int"));
			p3angle= (processingXML.p3angle.children().length() > 0) ? int(processingXML.p3angle) :  int(getDefaultValue("int"));
			p3radius= (processingXML.p3radius.children().length() > 0) ? int(processingXML.p3radius) :  int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			fillTriangle=  (processingXML.fillTriangle.children().length() > 0)  ?  getBooleanValue(processingXML.fillTriangle) : getBooleanValue("false");
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			fillColor= (processingXML.fillColor.children().length() > 0) ? String(processingXML.fillColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			transparancyPercentage= (processingXML.transparancyPercentage.children().length() > 0) ? String(processingXML.transparancyPercentage)  :  String(getDefaultValue("String"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var graphPlainTriangle_inst_30:graphPlainTriangle = lineToPlaceElement.Add_graphPlainTriangle(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,p1x,p1y,p2CoordinateType,p2x,p2y,p2angle,p2radius,p3CoordinateType,p3x,p3y,p3angle,p3radius,fillTriangle,styleColor,fillColor,hilightColor,transparancyPercentage,controlHeight);
			graphPlainTriangle_inst_30.alpha = 0;
			stageobj.addChild(graphPlainTriangle_inst_30);
			MovieClip(graphPlainTriangle_inst_30).addEventListener(Event.ENTER_FRAME, graphPlainTriangle_inst_30_AppearEvent);
			function graphPlainTriangle_inst_30_AppearEvent(event:Event):void {
				if (graphPlainTriangle_inst_30.alpha >= 1){
					graphPlainTriangle_inst_30.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, graphPlainTriangle_inst_30_AppearEvent);
				}else if(graphPlainTriangle_inst_30.alpha  < 0.5 ){
					graphPlainTriangle_inst_30.alpha = graphPlainTriangle_inst_30.alpha + 0.40;
				}else{
					graphPlainTriangle_inst_30.alpha = graphPlainTriangle_inst_30.alpha + 0.35;
				}
			}
			return graphPlainTriangle_inst_30;

		}



		public function Add_graphPlainCircle():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,x1:int,y1:int,radius:int,ringRadius:int,styleColor:String,fillCircle:Boolean,fillColor:String,hilightColor:String,transparancyPercentage:String,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			x1= (processingXML.x1.children().length() > 0) ? int(processingXML.x1) :  int(getDefaultValue("int"));
			y1= (processingXML.y1.children().length() > 0) ? int(processingXML.y1) :  int(getDefaultValue("int"));
			radius= (processingXML.radius.children().length() > 0) ? int(processingXML.radius) :  int(getDefaultValue("int"));
			ringRadius= (processingXML.ringRadius.children().length() > 0) ? int(processingXML.ringRadius) :  int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			fillCircle=  (processingXML.fillCircle.children().length() > 0)  ?  getBooleanValue(processingXML.fillCircle) : getBooleanValue("false");
			fillColor= (processingXML.fillColor.children().length() > 0) ? String(processingXML.fillColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			transparancyPercentage= (processingXML.transparancyPercentage.children().length() > 0) ? String(processingXML.transparancyPercentage)  :  String(getDefaultValue("String"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var graphPlainCircle_inst_31:graphPlainCircle = lineToPlaceElement.Add_graphPlainCircle(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,x1,y1,radius,ringRadius,styleColor,fillCircle,fillColor,hilightColor,transparancyPercentage,controlHeight);
			graphPlainCircle_inst_31.alpha = 0;
			stageobj.addChild(graphPlainCircle_inst_31);
			MovieClip(graphPlainCircle_inst_31).addEventListener(Event.ENTER_FRAME, graphPlainCircle_inst_31_AppearEvent);
			function graphPlainCircle_inst_31_AppearEvent(event:Event):void {
				if (graphPlainCircle_inst_31.alpha >= 1){
					graphPlainCircle_inst_31.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, graphPlainCircle_inst_31_AppearEvent);
				}else if(graphPlainCircle_inst_31.alpha  < 0.5 ){
					graphPlainCircle_inst_31.alpha = graphPlainCircle_inst_31.alpha + 0.40;
				}else{
					graphPlainCircle_inst_31.alpha = graphPlainCircle_inst_31.alpha + 0.35;
				}
			}
			return graphPlainCircle_inst_31;

		}



		public function Add_graphPlainEllipse():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,topLeftX:int,topLeftY:int,width:int,height:int,angleOfRotation:int,fillEllipse:Boolean,styleColor:String,fillColor:String,hilightColor:String,transparancyPercentage:String,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			topLeftX= (processingXML.topLeftX.children().length() > 0) ? int(processingXML.topLeftX) :  int(getDefaultValue("int"));
			topLeftY= (processingXML.topLeftY.children().length() > 0) ? int(processingXML.topLeftY) :  int(getDefaultValue("int"));
			width= (processingXML.width.children().length() > 0) ? int(processingXML.width) :  int(getDefaultValue("int"));
			height= (processingXML.height.children().length() > 0) ? int(processingXML.height) :  int(getDefaultValue("int"));
			angleOfRotation= (processingXML.angleOfRotation.children().length() > 0) ? int(processingXML.angleOfRotation) :  int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			fillEllipse=  (processingXML.fillEllipse.children().length() > 0)  ?  getBooleanValue(processingXML.fillEllipse) : getBooleanValue("false");
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			fillColor= (processingXML.fillColor.children().length() > 0) ? String(processingXML.fillColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			transparancyPercentage= (processingXML.transparancyPercentage.children().length() > 0) ? String(processingXML.transparancyPercentage)  :  String(getDefaultValue("String"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var graphPlainEllipse_inst_32:graphPlainEllipse = lineToPlaceElement.Add_graphPlainEllipse(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,topLeftX,topLeftY,width,height,angleOfRotation,fillEllipse,styleColor,fillColor,hilightColor,transparancyPercentage,controlHeight);
			graphPlainEllipse_inst_32.alpha = 0;
			stageobj.addChild(graphPlainEllipse_inst_32);
			MovieClip(graphPlainEllipse_inst_32).addEventListener(Event.ENTER_FRAME, graphPlainEllipse_inst_32_AppearEvent);
			function graphPlainEllipse_inst_32_AppearEvent(event:Event):void {
				if (graphPlainEllipse_inst_32.alpha >= 1){
					graphPlainEllipse_inst_32.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, graphPlainEllipse_inst_32_AppearEvent);
				}else if(graphPlainEllipse_inst_32.alpha  < 0.5 ){
					graphPlainEllipse_inst_32.alpha = graphPlainEllipse_inst_32.alpha + 0.40;
				}else{
					graphPlainEllipse_inst_32.alpha = graphPlainEllipse_inst_32.alpha + 0.35;
				}
			}
			return graphPlainEllipse_inst_32;

		}



		public function Add_graphPlainArc():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,centerX:int,centerY:int,radius:int,startingAngle:int,arcAngle:int,startingArrow:String,endingArrow:String,styleColor:String,hilightColor:String,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			centerX= (processingXML.centerX.children().length() > 0) ? int(processingXML.centerX) :  int(getDefaultValue("int"));
			centerY= (processingXML.centerY.children().length() > 0) ? int(processingXML.centerY) :  int(getDefaultValue("int"));
			radius= (processingXML.radius.children().length() > 0) ? int(processingXML.radius) :  int(getDefaultValue("int"));
			startingAngle= (processingXML.startingAngle.children().length() > 0) ? int(processingXML.startingAngle) :  int(getDefaultValue("int"));
			arcAngle= (processingXML.arcAngle.children().length() > 0) ? int(processingXML.arcAngle) :  int(getDefaultValue("int"));
			startingArrow= (processingXML.startingArrow.children().length() > 0) ? String(processingXML.startingArrow) :  String(getDefaultValue("String"));
			endingArrow= (processingXML.endingArrow.children().length() > 0) ? String(processingXML.endingArrow) :  String(getDefaultValue("String"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var graphPlainArc_inst_33:graphPlainArc = lineToPlaceElement.Add_graphPlainArc(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,centerX,centerY,radius,startingAngle,arcAngle,startingArrow,endingArrow,styleColor,hilightColor,controlHeight);
			graphPlainArc_inst_33.alpha = 0;
			stageobj.addChild(graphPlainArc_inst_33);
			MovieClip(graphPlainArc_inst_33).addEventListener(Event.ENTER_FRAME, graphPlainArc_inst_33_AppearEvent);
			function graphPlainArc_inst_33_AppearEvent(event:Event):void {
				if (graphPlainArc_inst_33.alpha >= 1){
					graphPlainArc_inst_33.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, graphPlainArc_inst_33_AppearEvent);
				}else if(graphPlainArc_inst_33.alpha  < 0.5 ){
					graphPlainArc_inst_33.alpha = graphPlainArc_inst_33.alpha + 0.40;
				}else{
					graphPlainArc_inst_33.alpha = graphPlainArc_inst_33.alpha + 0.35;
				}
			}
			return graphPlainArc_inst_33;

		}



		public function Add_ThreeDCube():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,x1:int,y1:int,sideLength:int,canRotate:Boolean,lineColor:String,side1Color:String,side2Color:String,side3Color:String,side4Color:String,side5Color:String,side6Color:String,hilightColor:String,transparancyPercentage:String,rotationAngle:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			x1= (processingXML.x1.children().length() > 0) ? int(processingXML.x1) :  int(getDefaultValue("int"));
			y1= (processingXML.y1.children().length() > 0) ? int(processingXML.y1) :  int(getDefaultValue("int"));
			sideLength= (processingXML.sideLength.children().length() > 0) ? int(processingXML.sideLength) :  int(getDefaultValue("int"));
			canRotate=  (processingXML.canRotate.children().length() > 0)  ? getBooleanValue(processingXML.canRotate) : getBooleanValue("false"); 
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			lineColor= (processingXML.lineColor.children().length() > 0) ? String(processingXML.lineColor)  :  String(getDefaultValue("String"));
			side1Color= (processingXML.side1Color.children().length() > 0) ? String(processingXML.side1Color)  :  String(getDefaultValue("String"));
			side2Color= (processingXML.side2Color.children().length() > 0) ? String(processingXML.side2Color)  :  String(getDefaultValue("String"));
			side3Color= (processingXML.side3Color.children().length() > 0) ? String(processingXML.side3Color)  :  String(getDefaultValue("String"));
			side4Color= (processingXML.side4Color.children().length() > 0) ? String(processingXML.side4Color)  :  String(getDefaultValue("String"));
			side5Color= (processingXML.side5Color.children().length() > 0) ? String(processingXML.side5Color)  :  String(getDefaultValue("String"));
			side6Color= (processingXML.side6Color.children().length() > 0) ? String(processingXML.side6Color)  :  String(getDefaultValue("String"));
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			transparancyPercentage= (processingXML.transparancyPercentage.children().length() > 0) ? String(processingXML.transparancyPercentage)  :  String(getDefaultValue("String"));
			rotationAngle= (processingXML.rotationAngle.children().length() > 0) ? int(processingXML.rotationAngle)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var ThreeDCube_inst_34:ThreeDCube = lineToPlaceElement.Add_ThreeDCube(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,x1,y1,sideLength,canRotate,lineColor,side1Color,side2Color,side3Color,side4Color,side5Color,side6Color,hilightColor,transparancyPercentage,rotationAngle,controlHeight);
			ThreeDCube_inst_34.alpha = 0;
			stageobj.addChild(ThreeDCube_inst_34);
			MovieClip(ThreeDCube_inst_34).addEventListener(Event.ENTER_FRAME, ThreeDCube_inst_34_AppearEvent);
			function ThreeDCube_inst_34_AppearEvent(event:Event):void {
				if (ThreeDCube_inst_34.alpha >= 1){
					ThreeDCube_inst_34.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, ThreeDCube_inst_34_AppearEvent);
				}else if(ThreeDCube_inst_34.alpha  < 0.5 ){
					ThreeDCube_inst_34.alpha = ThreeDCube_inst_34.alpha + 0.40;
				}else{
					ThreeDCube_inst_34.alpha = ThreeDCube_inst_34.alpha + 0.35;
				}
			}
			return ThreeDCube_inst_34;

		}



		public function Add_ThreeDSphere():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,centerX:int,centerY:int,radius:int,canRotate:Boolean,verticalCutPercentage:String,horizontalCutPercentage:String,showSegmentLine:Boolean,hilightColor:String,transparancyPercentage:String,rotationAngle:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			centerX= (processingXML.centerX.children().length() > 0) ? int(processingXML.centerX) :  int(getDefaultValue("int"));
			centerY= (processingXML.centerY.children().length() > 0) ? int(processingXML.centerY) :  int(getDefaultValue("int"));
			radius= (processingXML.radius.children().length() > 0) ? int(processingXML.radius) :  int(getDefaultValue("int"));
			canRotate=  (processingXML.canRotate.children().length() > 0)  ? getBooleanValue(processingXML.canRotate) : getBooleanValue("false"); 
			verticalCutPercentage= (processingXML.verticalCutPercentage.children().length() > 0) ? String(processingXML.verticalCutPercentage) :  String(getDefaultValue("String"));
			horizontalCutPercentage= (processingXML.horizontalCutPercentage.children().length() > 0) ? String(processingXML.horizontalCutPercentage) :  String(getDefaultValue("String"));
			showSegmentLine=  (processingXML.showSegmentLine.children().length() > 0)  ? getBooleanValue(processingXML.showSegmentLine) : getBooleanValue("false"); 
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			transparancyPercentage= (processingXML.transparancyPercentage.children().length() > 0) ? String(processingXML.transparancyPercentage)  :  String(getDefaultValue("String"));
			rotationAngle= (processingXML.rotationAngle.children().length() > 0) ? int(processingXML.rotationAngle)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var ThreeDSphere_inst_35:ThreeDSphere = lineToPlaceElement.Add_ThreeDSphere(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,centerX,centerY,radius,canRotate,verticalCutPercentage,horizontalCutPercentage,showSegmentLine,hilightColor,transparancyPercentage,rotationAngle,controlHeight);
			ThreeDSphere_inst_35.alpha = 0;
			stageobj.addChild(ThreeDSphere_inst_35);
			MovieClip(ThreeDSphere_inst_35).addEventListener(Event.ENTER_FRAME, ThreeDSphere_inst_35_AppearEvent);
			function ThreeDSphere_inst_35_AppearEvent(event:Event):void {
				if (ThreeDSphere_inst_35.alpha >= 1){
					ThreeDSphere_inst_35.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, ThreeDSphere_inst_35_AppearEvent);
				}else if(ThreeDSphere_inst_35.alpha  < 0.5 ){
					ThreeDSphere_inst_35.alpha = ThreeDSphere_inst_35.alpha + 0.40;
				}else{
					ThreeDSphere_inst_35.alpha = ThreeDSphere_inst_35.alpha + 0.35;
				}
			}
			return ThreeDSphere_inst_35;

		}



		public function Add_NewFeature():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,centerXY:int,centerY:int,radius:int,canRotate:Boolean,verticalCutPercentage:String,horizontalCutPercentage:String,showSegmentLine:Boolean,hilightColor:String,transparancyPercentage:String,rotationAngle:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			centerXY= (processingXML.centerXY.children().length() > 0) ? int(processingXML.centerXY) :  int(getDefaultValue("int"));
			centerY= (processingXML.centerY.children().length() > 0) ? int(processingXML.centerY) :  int(getDefaultValue("int"));
			radius= (processingXML.radius.children().length() > 0) ? int(processingXML.radius) :  int(getDefaultValue("int"));
			canRotate=  (processingXML.canRotate.children().length() > 0)  ? getBooleanValue(processingXML.canRotate) : getBooleanValue("false"); 
			verticalCutPercentage= (processingXML.verticalCutPercentage.children().length() > 0) ? String(processingXML.verticalCutPercentage) :  String(getDefaultValue("String"));
			horizontalCutPercentage= (processingXML.horizontalCutPercentage.children().length() > 0) ? String(processingXML.horizontalCutPercentage) :  String(getDefaultValue("String"));
			showSegmentLine=  (processingXML.showSegmentLine.children().length() > 0)  ? getBooleanValue(processingXML.showSegmentLine) : getBooleanValue("false"); 
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			hilightColor= (processingXML.hilightColor.children().length() > 0) ? String(processingXML.hilightColor)  :  String(getDefaultValue("String"));
			transparancyPercentage= (processingXML.transparancyPercentage.children().length() > 0) ? String(processingXML.transparancyPercentage)  :  String(getDefaultValue("String"));
			rotationAngle= (processingXML.rotationAngle.children().length() > 0) ? int(processingXML.rotationAngle)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var NewFeature_inst_36:NewFeature = lineToPlaceElement.Add_NewFeature(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,centerXY,centerY,radius,canRotate,verticalCutPercentage,horizontalCutPercentage,showSegmentLine,hilightColor,transparancyPercentage,rotationAngle,controlHeight);
			NewFeature_inst_36.alpha = 0;
			stageobj.addChild(NewFeature_inst_36);
			MovieClip(NewFeature_inst_36).addEventListener(Event.ENTER_FRAME, NewFeature_inst_36_AppearEvent);
			function NewFeature_inst_36_AppearEvent(event:Event):void {
				if (NewFeature_inst_36.alpha >= 1){
					NewFeature_inst_36.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, NewFeature_inst_36_AppearEvent);
				}else if(NewFeature_inst_36.alpha  < 0.5 ){
					NewFeature_inst_36.alpha = NewFeature_inst_36.alpha + 0.40;
				}else{
					NewFeature_inst_36.alpha = NewFeature_inst_36.alpha + 0.35;
				}
			}
			return NewFeature_inst_36;

		}



		public function Add_voiceLink():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,fileToLink:String,styleColor:String,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			fileToLink= (processingXML.fileToLink.children().length() > 0) ? String(processingXML.fileToLink) :  String(getDefaultValue("String"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var voiceLink_inst_37:voiceLink = lineToPlaceElement.Add_voiceLink(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,fileToLink,styleColor,controlHeight);
			voiceLink_inst_37.alpha = 0;
			stageobj.addChild(voiceLink_inst_37);
			MovieClip(voiceLink_inst_37).addEventListener(Event.ENTER_FRAME, voiceLink_inst_37_AppearEvent);
			function voiceLink_inst_37_AppearEvent(event:Event):void {
				if (voiceLink_inst_37.alpha >= 1){
					voiceLink_inst_37.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, voiceLink_inst_37_AppearEvent);
				}else if(voiceLink_inst_37.alpha  < 0.5 ){
					voiceLink_inst_37.alpha = voiceLink_inst_37.alpha + 0.40;
				}else{
					voiceLink_inst_37.alpha = voiceLink_inst_37.alpha + 0.35;
				}
			}
			return voiceLink_inst_37;

		}



		public function Add_highlightBox():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,x1:int,y1:int,length:int,height:int,explanationText:String,explanationTextXLocation:int,explanationTextYLocation:int,explanationTextWidth:int,explanationTextHeight:int,styleColor:String,fillColor:String,dissolve:Boolean,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			x1= (processingXML.x1.children().length() > 0) ? int(processingXML.x1) :  int(getDefaultValue("int"));
			y1= (processingXML.y1.children().length() > 0) ? int(processingXML.y1) :  int(getDefaultValue("int"));
			length= (processingXML.length.children().length() > 0) ? int(processingXML.length) :  int(getDefaultValue("int"));
			height= (processingXML.height.children().length() > 0) ? int(processingXML.height) :  int(getDefaultValue("int"));
			explanationText= (processingXML.explanationText.children().length() > 0) ? String(processingXML.explanationText) :  String(getDefaultValue("String"));
			explanationTextXLocation= (processingXML.explanationTextXLocation.children().length() > 0) ? int(processingXML.explanationTextXLocation) :  int(getDefaultValue("int"));
			explanationTextYLocation= (processingXML.explanationTextYLocation.children().length() > 0) ? int(processingXML.explanationTextYLocation) :  int(getDefaultValue("int"));
			explanationTextWidth= (processingXML.explanationTextWidth.children().length() > 0) ? int(processingXML.explanationTextWidth) :  int(getDefaultValue("int"));
			explanationTextHeight= (processingXML.explanationTextHeight.children().length() > 0) ? int(processingXML.explanationTextHeight) :  int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			styleColor= (processingXML.styleColor.children().length() > 0) ? String(processingXML.styleColor)  :  String(getDefaultValue("String"));
			fillColor= (processingXML.fillColor.children().length() > 0) ? String(processingXML.fillColor)  :  String(getDefaultValue("String"));
			dissolve=  (processingXML.dissolve.children().length() > 0)  ?  getBooleanValue(processingXML.dissolve) : getBooleanValue("false");
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var highlightBox_inst_38:highlightBox = lineToPlaceElement.Add_highlightBox(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,x1,y1,length,height,explanationText,explanationTextXLocation,explanationTextYLocation,explanationTextWidth,explanationTextHeight,styleColor,fillColor,dissolve,controlHeight);
			highlightBox_inst_38.alpha = 0;
			stageobj.addChild(highlightBox_inst_38);
			MovieClip(highlightBox_inst_38).addEventListener(Event.ENTER_FRAME, highlightBox_inst_38_AppearEvent);
			function highlightBox_inst_38_AppearEvent(event:Event):void {
				if (highlightBox_inst_38.alpha >= 1){
					highlightBox_inst_38.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, highlightBox_inst_38_AppearEvent);
				}else if(highlightBox_inst_38.alpha  < 0.5 ){
					highlightBox_inst_38.alpha = highlightBox_inst_38.alpha + 0.40;
				}else{
					highlightBox_inst_38.alpha = highlightBox_inst_38.alpha + 0.35;
				}
			}
			return highlightBox_inst_38;

		}



		public function Add_operateExistingControl():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,operatedControlName:String,operation:String,moveXUnit:int,moveYUnit:int,rotateAngle:int,durationInSecond:int,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			operatedControlName= (processingXML.operatedControlName.children().length() > 0) ? String(processingXML.operatedControlName) :  String(getDefaultValue("String"));
			operation= (processingXML.operation.children().length() > 0) ? String(processingXML.operation) :  String(getDefaultValue("String"));
			moveXUnit= (processingXML.moveXUnit.children().length() > 0) ? int(processingXML.moveXUnit) :  int(getDefaultValue("int"));
			moveYUnit= (processingXML.moveYUnit.children().length() > 0) ? int(processingXML.moveYUnit) :  int(getDefaultValue("int"));
			rotateAngle= (processingXML.rotateAngle.children().length() > 0) ? int(processingXML.rotateAngle) :  int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			durationInSecond= (processingXML.durationInSecond.children().length() > 0) ? int(processingXML.durationInSecond)  :  int(getDefaultValue("int"));
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var operateExistingControl_inst_39:operateExistingControl = lineToPlaceElement.Add_operateExistingControl(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,operatedControlName,operation,moveXUnit,moveYUnit,rotateAngle,durationInSecond,controlHeight);
			operateExistingControl_inst_39.alpha = 0;
			stageobj.addChild(operateExistingControl_inst_39);
			MovieClip(operateExistingControl_inst_39).addEventListener(Event.ENTER_FRAME, operateExistingControl_inst_39_AppearEvent);
			function operateExistingControl_inst_39_AppearEvent(event:Event):void {
				if (operateExistingControl_inst_39.alpha >= 1){
					operateExistingControl_inst_39.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, operateExistingControl_inst_39_AppearEvent);
				}else if(operateExistingControl_inst_39.alpha  < 0.5 ){
					operateExistingControl_inst_39.alpha = operateExistingControl_inst_39.alpha + 0.40;
				}else{
					operateExistingControl_inst_39.alpha = operateExistingControl_inst_39.alpha + 0.35;
				}
			}
			return operateExistingControl_inst_39;

		}



		public function Add_multiStepLine():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,stepDescriptions:String,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			stepDescriptions= (processingXML.stepDescriptions.children().length() > 0) ? String(processingXML.stepDescriptions) :  String(getDefaultValue("String"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var multiStepLine_inst_40:multiStepLine = lineToPlaceElement.Add_multiStepLine(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,stepDescriptions,controlHeight);
			multiStepLine_inst_40.alpha = 0;
			stageobj.addChild(multiStepLine_inst_40);
			MovieClip(multiStepLine_inst_40).addEventListener(Event.ENTER_FRAME, multiStepLine_inst_40_AppearEvent);
			function multiStepLine_inst_40_AppearEvent(event:Event):void {
				if (multiStepLine_inst_40.alpha >= 1){
					multiStepLine_inst_40.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, multiStepLine_inst_40_AppearEvent);
				}else if(multiStepLine_inst_40.alpha  < 0.5 ){
					multiStepLine_inst_40.alpha = multiStepLine_inst_40.alpha + 0.40;
				}else{
					multiStepLine_inst_40.alpha = multiStepLine_inst_40.alpha + 0.35;
				}
			}
			return multiStepLine_inst_40;

		}



		public function Add_executeMultiStep():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,controlName:String,stepID:String,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			controlName= (processingXML.controlName.children().length() > 0) ? String(processingXML.controlName) :  String(getDefaultValue("String"));
			stepID= (processingXML.stepID.children().length() > 0) ? String(processingXML.stepID) :  String(getDefaultValue("String"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var executeMultiStep_inst_41:executeMultiStep = lineToPlaceElement.Add_executeMultiStep(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,controlName,stepID,controlHeight);
			executeMultiStep_inst_41.alpha = 0;
			stageobj.addChild(executeMultiStep_inst_41);
			MovieClip(executeMultiStep_inst_41).addEventListener(Event.ENTER_FRAME, executeMultiStep_inst_41_AppearEvent);
			function executeMultiStep_inst_41_AppearEvent(event:Event):void {
				if (executeMultiStep_inst_41.alpha >= 1){
					executeMultiStep_inst_41.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, executeMultiStep_inst_41_AppearEvent);
				}else if(executeMultiStep_inst_41.alpha  < 0.5 ){
					executeMultiStep_inst_41.alpha = executeMultiStep_inst_41.alpha + 0.40;
				}else{
					executeMultiStep_inst_41.alpha = executeMultiStep_inst_41.alpha + 0.35;
				}
			}
			return executeMultiStep_inst_41;

		}



		public function Add_executeMultiStepHH():Object{

			var tweenStart_x:int,tweenStart_y:int,tweenEnd_x:int,tweenEnd_y:int,tweenStyle:String,AnimateTime_:int,controlName:String,stepID:String,controlHeight:int;

			var processingXML:XMLList;

			//EXTRACT POSITION PARAMETER

			processingXML = elementData.position;

			tweenStart_x=(processingXML.hasOwnProperty("@p1")) ? int(processingXML.attribute("p1")) : int(getDefaultValue("int"));
			tweenStart_y=(processingXML.hasOwnProperty("@p2")) ? int(processingXML.attribute("p2")) : int(getDefaultValue("int"));
			tweenEnd_x=(processingXML.hasOwnProperty("@p3")) ? int(processingXML.attribute("p3")) : int(getDefaultValue("int"));
			tweenEnd_y=(processingXML.hasOwnProperty("@p4")) ? int(processingXML.attribute("p4")) : int(getDefaultValue("int"));
			tweenStyle=(processingXML.hasOwnProperty("@p5")) ? String(processingXML.attribute("p5")) : String(getDefaultValue("String"));
			AnimateTime_=(processingXML.hasOwnProperty("@p6")) ? int(processingXML.attribute("p6")) : int(getDefaultValue("int"));
			processingXML = null;

			//EXTRACT HEIGHT PARAMETER
			var height = elementData.height;
			controlHeight=height;
			processingXML = null;

			//EXTRACT CONTENT PARAMETER
			processingXML = elementData.content;
			controlName= (processingXML.controlName.children().length() > 0) ? String(processingXML.controlName) :  String(getDefaultValue("String"));
			stepID= (processingXML.stepID.children().length() > 0) ? String(processingXML.stepID) :  String(getDefaultValue("String"));
			processingXML = null;

			//EXTRACT DISPLAY PARAMETER
			processingXML = elementData.display;
			processingXML = null;

			if(getGlobalTweenStyle() == "none"){
				tweenStyle = "none"; 
			}

			var executeMultiStepHH_inst_42:executeMultiStepHH = lineToPlaceElement.Add_executeMultiStepHH(tweenStart_x,tweenStart_y,tweenEnd_x,tweenEnd_y,tweenStyle,AnimateTime_,controlName,stepID,controlHeight);
			executeMultiStepHH_inst_42.alpha = 0;
			stageobj.addChild(executeMultiStepHH_inst_42);
			MovieClip(executeMultiStepHH_inst_42).addEventListener(Event.ENTER_FRAME, executeMultiStepHH_inst_42_AppearEvent);
			function executeMultiStepHH_inst_42_AppearEvent(event:Event):void {
				if (executeMultiStepHH_inst_42.alpha >= 1){
					executeMultiStepHH_inst_42.alpha  = 1;
					MovieClip(event.target).removeEventListener(Event.ENTER_FRAME, executeMultiStepHH_inst_42_AppearEvent);
				}else if(executeMultiStepHH_inst_42.alpha  < 0.5 ){
					executeMultiStepHH_inst_42.alpha = executeMultiStepHH_inst_42.alpha + 0.40;
				}else{
					executeMultiStepHH_inst_42.alpha = executeMultiStepHH_inst_42.alpha + 0.35;
				}
			}
			return executeMultiStepHH_inst_42;

		}


	}
}
