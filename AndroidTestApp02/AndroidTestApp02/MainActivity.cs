﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;
using Android.Content;
using Android.Graphics;
using Android.Animation;

using Android.Graphics.Drawables; //directive added for shape drawable
using Android.Graphics.Drawables.Shapes; //DIRECTIVE USED FOR shape object used for the constructor in shapeDrawable class 


using AndroidTestApp02.SumAnimatorTrial.DrawableObjects;
using AndroidTestApp02.SumAnimatorTrial.DrawableObjects.DrawableObjectVOs;


//GENERAL RESOURCES
//  https://developer.xamarin.com/api/type/Android.Views.View/
//  https://developer.xamarin.com/guides/android/application_fundamentals/activity_lifecycle/
//GENERAL DETAILS ABOUT ANDRIOD DEVELOPMENT (VERY GOOD): http://www.techotopia.com/index.php/Android_4_App_Development_Essentials
//GENERAL DETAILS ABOUT ANDRIOD DEVELOPMENT (VERY GOOD):http://www.vogella.com/tutorials/AndroidCustomViews/article.html
//GENERAL DETAILS ABOUT VIEWLAYOUTS (GOOD): https://arpitonline.com/2012/07/01/creating-custom-layouts-for-android/


namespace AndroidTestApp02
{
    [Activity(Label = "AndroidTestApp02", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity, GestureDetector.IOnGestureListener
    {

        /* documentation: https://developer.xamarin.com/guides/android/application_fundamentals/activity_lifecycle/ */

        DrawBasedOnView myDrawBasedOnView;
        Context currentCOntext;


        DrawBasedOnShapeDrawable myDrawBasedOnShapeDrawable;


        SumAnimatorTrial.SumAnimatorStage AnimatorStage;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            // SetContentView (Resource.Layout.Main);
                        //this.SetContentView(Resource.Layout.Main);  // copied from  https://developer.xamarin.com/guides/android/application_fundamentals/activity_lifecycle/  - donot know what this does
                        

            //CODE ADDED BY HH TO SEE IF THE VIEW CAN BE LINCKED TO THE MAIN ACTIVITY

            currentCOntext = this.ApplicationContext;


            DrawBasedOnViewGroup vwGroup = new DrawBasedOnViewGroup(currentCOntext);
            


            Paint green = new Paint
            {
                AntiAlias = true,
                Color = Color.Rgb(0x99, 0xcc, 0),
            };
            myDrawBasedOnView = new DrawBasedOnView(currentCOntext, 50, 50, green);
            //this.SetContentView(myDrawBasedOnView);


            //THIS CLASS DOES NOT SEEM TO WORK
            //myDrawBasedOnShapeDrawable = new DrawBasedOnShapeDrawable(currentCOntext);
            //this.SetContentView(myDrawBasedOnShapeDrawable);


            Paint newColor = new Paint
            {
                AntiAlias = true,
                Color = Color.Rgb(0x99, 0xcc, 0xee),
            };
            DrawBasedOnView myDrawBasedOnView1 = new DrawBasedOnView(currentCOntext, 1000, 200, newColor);
            //this.SetContentView(myDrawBasedOnView1);



            AnimatorStage = new SumAnimatorTrial.SumAnimatorStage(currentCOntext);


            object rectParam = new RectangleVO(100, 100, 700, 200, 150, 0xff, 0x00, 0x00, 0x00, 0xff, 0x00);
            View rct = (View)AnimatorStage.AddDrawableObject<Rectangle>("rectangle", rectParam);

            object circleParam = new CircleVO(300, 300, 100, 150, 0x00, 0xff, 0x00, 0x00, 0xff, 0x00);
            View crcl = (View)AnimatorStage.AddDrawableObject<Circle>("circle", circleParam);


           //vwGroup.AddView(myDrawBasedOnView);
            vwGroup.AddView(myDrawBasedOnView1);
            //vwGroup.AddView(AnimatorObjectsView);

            vwGroup.AddView(rct);
            vwGroup.AddView(crcl);


            this.SetContentView(vwGroup);

        }


        protected override void OnResume()
        {
            base.OnResume();

        }

       
        public bool OnDown(MotionEvent e)
        {
            return false;
        }
         public bool OnFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
        {
            //_textView.Text = string.Format("Fling velocity: {0} x {1}", velocityX, velocityY);
            return true;
        }
        public void OnLongPress(MotionEvent e) { }
        public bool OnScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
        {
            return false;
        }
        public void OnShowPress(MotionEvent e) { }
        public bool OnSingleTapUp(MotionEvent e)
        {
            return false;
        }
        public override bool OnTouchEvent(MotionEvent e)
        {

            //WORKING CODE FOR ONE TRANSITION
            //ObjectAnimator animator = ObjectAnimator.OfFloat(myDrawBasedOnView, "translationX", 200f);
            
            /*            ObjectAnimator animator = ObjectAnimator.OfFloat(AnimatorObjectsView, "translationX",  200f);
                        animator.SetDuration(1000);
                        ObjectAnimator animatory = ObjectAnimator.OfFloat(AnimatorObjectsView, "translationY", 200f);
                        animatory.SetDuration(1000);
                        animator.Start();
                        animatory.Start();
                        */
            /*
                        ObjectAnimator animatorx_2 = ObjectAnimator.OfFloat(myDrawBasedOnShapeDrawable, "translationX", 200f);
                        animatorx_2.SetDuration(1000);
                        ObjectAnimator animatory_2 = ObjectAnimator.OfFloat(myDrawBasedOnShapeDrawable, "translationY", 200f);
                        animatory_2.SetDuration(1000);
                        animatorx_2.Start();
                        animatory_2.Start();

            */
            




            return true;





        }

        /**/
    }



    public class DrawBasedOnViewGroup : ViewGroup
    {//   THIS CLASS USED THE VIEW GROUP TO CONTAIN MULTIPLE VIEWS


        /** The amount of space used by children in the left gutter. */
        private int mLeftWidth;

        /** The amount of space used by children in the right gutter. */
        private int mRightWidth;
    
        /** These are used for computing child frames based on their gravity. */
        private  Rect mTmpContainerRect = new Rect();
        private  Rect mTmpChildRect = new Rect();

        public DrawBasedOnViewGroup(Context context) : base(context)
        {
            return;
        }

        /**
         * Any layout manager that doesn't scroll will want this.
         */
        public override bool  ShouldDelayChildPressedState()
        {
            return false;
        }


        /**
         * Position all children within this layout.
         */
        protected override void OnLayout(bool changed, int left, int top, int right, int bottom)
        {
            int count = ChildCount;

            // These are the far left and right edges in which we are performing layout.
            int leftPos = PaddingLeft;
            int rightPos = right - left - PaddingRight;

            // This is the middle region inside of the gutter.
            int middleLeft = leftPos + mLeftWidth;
            int middleRight = rightPos - mRightWidth;

            // These are the top and bottom edges in which we are performing layout.
            int parentTop = PaddingTop;
            int parentBottom = bottom - top - PaddingBottom;

            int itemWidth = 1;
            if (this.ChildCount > 0) { 
                itemWidth = (right - left) / this.ChildCount;
            }
            for (int i = 0; i < this.ChildCount; i++)
            {
                View v = GetChildAt(i);
                //v.Layout(itemWidth * i, top, (i + 1) * itemWidth, bottom);
                //v.Layout(itemWidth * i, 0, (i + 1) * itemWidth, bottom - top);
                //v.Layout(itemWidth * i, top, (i + 1) * itemWidth, bottom);
                //v.Layout(itemWidth * i, 0, (i + 1) * itemWidth, bottom - top);

                v.Layout(0, 0, (i + 1) * itemWidth, bottom - top);


            }




            //THIS FOR LOOP IS EXPECTED TO BE REPLACED BY THE FOR LOOP AT THE TOP
            /*
            for (int i = 0; i < count; i++)
            {
                 View child = GetChildAt(i);
                if (child.Visibility !=  ViewStates.Gone)
                {
                    LayoutParams lp = (LayoutParams)child.LayoutParameters;

                    int width = child.MeasuredWidth;
                    int height = child.MeasuredHeight;

                    

                    // Compute the frame in which we are placing this child.
                    if (lp. == LayoutParams.POSITION_LEFT)
                    {
                        mTmpContainerRect.left = leftPos + lp.leftMargin;
                        mTmpContainerRect.right = leftPos + width + lp.rightMargin;
                        leftPos = mTmpContainerRect.right;
                    }
                    else if (lp.position == LayoutParams.POSITION_RIGHT)
                    {
                        mTmpContainerRect.right = rightPos - lp.rightMargin;
                        mTmpContainerRect.left = rightPos - width - lp.leftMargin;
                        rightPos = mTmpContainerRect.left;
                    }
                    else
                    {
                        mTmpContainerRect.left = middleLeft + lp.leftMargin;
                        mTmpContainerRect.right = middleRight - lp.rightMargin;
                    }
                    mTmpContainerRect.top = parentTop + lp.topMargin;
                    mTmpContainerRect.bottom = parentBottom - lp.bottomMargin;

                    // Use the child's gravity and size to determine its final
                    // frame within its container.
                    Gravity.apply(lp.gravity, width, height, mTmpContainerRect, mTmpChildRect);
                    

                    // Place the child.
                    //child.layout(mTmpChildRect.left, mTmpChildRect.top, mTmpChildRect.right, mTmpChildRect.bottom); //this code is copied from https://developer.android.com/reference/android/view/ViewGroup.html and seems to be not applicable
                    child.Layout(0,0, 10, 10);
                }
               

            }



            */
        }

    }


        public class DrawBasedOnView : View
    {//      THIS CLASS PROVIDES THE DRAW ON THE VIWE USING CANVAS

        // DOCUMENTATION ON ANDROID DEVELOPMENT SDK - https://developer.android.com/reference/android/view/View.html

        float _Left = 0;
        float _Top = 0;
        Paint _fillColor;

        public DrawBasedOnView(Context c, float left, float Top, Paint fillColor) : base(c)
        {

            _Top = Top;
            _Left = left;
            _fillColor = fillColor;
            return;
        }

        
        protected override void OnDraw(Canvas canvas)
        {
            base.OnDraw(canvas);

            Canvas _canvasInstance;

            _canvasInstance = canvas;

            Paint green = new Paint
            {
                AntiAlias = true,
                Color = Color.Rgb(0x99, 0xcc, 0xcc),
            };
            green.SetStyle(Paint.Style.FillAndStroke);

            Paint red = new Paint
            {
                AntiAlias = true,
                Color = Color.Rgb(0xff, 0x44, 0x44)
            };
            red.SetStyle(Paint.Style.Stroke);

            float middle = canvas.Width * 0.25f;
            canvas.DrawPaint(red);
            //canvas.DrawRect(0, 0, middle, canvas.Height, green);

            this.Alpha = 0.5f;
            
            _fillColor.SetStyle(Paint.Style.FillAndStroke);

            DrawRect(canvas, _Left, _Top, _Left + 200, _Top +  200, _fillColor);


        }

          


        //local procedure created by HH
        private void DrawRect(Canvas _Canvas, float left, float top, float right, float bottom, Paint paintBrush)
        {
            //canvas.DrawPaint(red);
            //canvas.DrawRect(0, 0, middle, canvas.Height, green);

            _Canvas.DrawRect( left, top, right, bottom, paintBrush);
            
            return;
        }
        
        //DOCUMENTATION ON THE ANIMATION : https://developer.android.com/guide/topics/graphics/overview.html
        
    }


    public class DrawBasedOnShapeDrawable : View
    {//      THIS CLASS PROVIDES THE DRAW ON THE CUSTOM DRAWBALE USING ShapeDrawable

        // DOCUMENTATION ON THE DETAILS PRESENTED IN THE SHAPE DRAWABLE SECTION IN THE PAGE: - https://developer.android.com/guide/topics/graphics/2d-graphics.html

        private ShapeDrawable mDrawable;


        public DrawBasedOnShapeDrawable(Context c) : base(c)
        {
            return;
        }

        public void drawUsingCustomDrawable()
        {

            int x = 200;
            int y = 200;
            int width = 500;
            int height = 500;


            this.Alpha = 0.5f;

            mDrawable = new ShapeDrawable(new  OvalShape());
            // If the color isn't set, the shape uses black as the default.
            mDrawable.Paint.SetARGB(200, 89, 45, 67);
            // If the bounds aren't set, the shape can't be drawn.
            mDrawable.Bounds.Set(x, y, width, height);

           

            return;
        }








        protected override void OnDraw(Canvas canvas)
        {
           

            Canvas _canvasInstance;

            _canvasInstance = canvas;

            drawUsingCustomDrawable();

           mDrawable.Draw(canvas);

            base.OnDraw(canvas);
        }


        

    }


}

