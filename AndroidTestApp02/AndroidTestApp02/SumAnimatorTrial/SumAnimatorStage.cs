﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;


using Android.Graphics;
using Android.Animation;

using System.Collections;

using Java;

using AndroidTestApp02.SumAnimatorTrial.DrawableObjects;
using AndroidTestApp02.SumAnimatorTrial.DrawableObjects.DrawableObjectVOs;


//using System.Json;

using Org.Json;



using Android.Util;



/*
 *   this class is supposed to have the logic for expanding and contracting the stage
 * 
 * 
 * 
 */
namespace AndroidTestApp02.SumAnimatorTrial
{
    class SumAnimatorStage 
    {
        
        IDictionary<string,object> _drawableObjectsDictionary;

        Context _context;

        CreateDrawableObject CreateDrawableObj;


        public SumAnimatorStage(Context context) 
        {


            _drawableObjectsDictionary = new Dictionary<string, object>();
            _context = context;

            CreateDrawableObj = new CreateDrawableObject();

            return;
        }


        public Java.Lang.Object AddDrawableObject<T>(string ObjectName, object objParam)
        {

            string json1 = "{ Name: 'Bob', HairColor: 'Brown' }";

            JSONObject obj = new JSONObject(json1);

            //string s = obj.GetString("Name");

            try
            {




                String newString = "{\"widget\":{\"debug\": \"on\",\"window\": {\"title\": \"Sample Konfabulator Widget\",\"name\": \"main_window\",\"width\": 500,\"height\": 500},\"image\": {\"src\": \"Images/Sun.png\",\"name\": \"sun1\",\"hOffset\": 250,\"vOffset\": 250,\"alignment\": \"center\"},\"text\": {\"data\": \"Click Here\",\"size\": 36,\"style\": \"bold\",\"name\": \"text1\",\"hOffset\": 250,\"vOffset\": 100,\"alignment\": \"center\",\"onMouseUp\": \"sun1.opacity = (sun1.opacity / 100) * 90;\"}}}";

                JSONObject jsonObjectNew = new JSONObject(newString).GetJSONObject("widget");
                String nameNew = jsonObjectNew.GetString("debug");
                String nameNew1 = jsonObjectNew.GetString("window");
                //JSONArray jArrayNew = jsonObjectNew.GetJSONArray("employee");
                //String dNew = jArrayNew.GetJSONObject(1).GetString("name");

                String ParsingData = "{\"employee\":[{\"name\":\"amir\",\"salary\":56000,\"married\":false},{\"name\":\"hari\",\"salary\":57000,\"married\":true}]}";
                JSONObject jsonObject = new JSONObject(ParsingData).GetJSONObject("employee");
                String name = jsonObject.GetString("name");
                int salary = jsonObject.GetInt("salary");
                JSONArray jArray = jsonObject.GetJSONArray("employee");
                String d = jArray.GetJSONObject(1).GetString("name");

            }
            catch (JSONException e)
            {


            }




            Java.Lang.Object createdObject = CreateDrawableObj.CreateObject(_context, ObjectName, objParam);
            _drawableObjectsDictionary.Add(ObjectName, createdObject);
            return createdObject;
           
        }

        
    }
}