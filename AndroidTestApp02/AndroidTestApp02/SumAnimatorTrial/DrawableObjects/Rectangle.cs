﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Android.Animation;
using Android.Graphics;


using AndroidTestApp02.SumAnimatorTrial.Animation;
using AndroidTestApp02.SumAnimatorTrial.DrawableObjects.DrawableObjectVOs;


namespace AndroidTestApp02.SumAnimatorTrial.DrawableObjects
{
    class Rectangle : View
    {
        
        RectangleVO RectangleParams;

        ANDSumAnimationAnimator _sumAnimationAnimator;


        public Rectangle(Context context, RectangleVO rectangleVO) : base(context)
        {

            RectangleParams = rectangleVO;

            _sumAnimationAnimator = new ANDSumAnimationAnimator();


            return;
        }

        protected override void OnDraw(Canvas canvas)
        {
            base.OnDraw(canvas);
            
            canvas.DrawRect(RectangleParams.Left,RectangleParams.Top, RectangleParams.Right, RectangleParams.Bottom, RectangleParams.IneteriorPaint);

            _sumAnimationAnimator.AnimateMove(this, 200f, 200f);
            return;
        }




        /* THEORY

         1.     For a view with a frequently changing alpha, such as during a fading animation, it is strongly recommended for performance reasons to either override View.HasOverlappingRendering to return false if appropriate, or setting a View.SetLayerType(LayerType,Paint) on the view for the duration of the animation..... [https://developer.xamarin.com/api/property/Android.Views.View.Alpha/]



         */
    }
}