﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Android.Graphics;

namespace AndroidTestApp02.SumAnimatorTrial.DrawableObjects.DrawableObjectVOs
{
    class CircleVO
    {

        public CircleVO(float centerX, float centerY, float radius, Int32 opacity, int interiorRed, int interiorBlue, int interiorGreen, int borderRed, int borderBlue, int borderGreen)
        {
            CenterX = centerX;
            CenterY = centerY;
            Radius = radius;
            
            Opacity = opacity;

            _ineteriorPaint = new Paint
            {
                AntiAlias = true,
                Color = Color.Rgb(interiorRed, interiorBlue, interiorGreen),
                Alpha = Opacity,
            };
            _ineteriorPaint.SetStyle(Paint.Style.FillAndStroke);

            _borderPaint = new Paint
            {
                AntiAlias = true,
                Color = Color.Rgb(borderRed, borderBlue, borderGreen),
                Alpha = Opacity
            };
            _borderPaint.SetStyle(Paint.Style.Stroke);


        }


        public float CenterX { get; }
        public float CenterY { get; }
        public float Radius { get; }
        public Int32 Opacity { get; }

        private Paint _ineteriorPaint;
        public Paint IneteriorPaint { get { return _ineteriorPaint; } }

        private Paint _borderPaint;
        public Paint BorderPaint { get { return _borderPaint; } }


    }
}