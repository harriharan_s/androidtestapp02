﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using AndroidTestApp02.SumAnimatorTrial.DrawableObjects.DrawableObjectVOs;


namespace AndroidTestApp02.SumAnimatorTrial.DrawableObjects
{
    class CreateDrawableObject
    {


        public Java.Lang.Object CreateObject(Context C, string ObjectName, object parameters)
        {

            switch (ObjectName)
            {
                case "rectangle":
                    Rectangle rectangle = new Rectangle(C,(RectangleVO) parameters);
                    return rectangle;
                    break;

                case "circle":
                    Circle circle = new Circle(C, (CircleVO) parameters);
                    return circle;
                    break;

                default:
                    break;
            }

            return null;

        }


        }
}