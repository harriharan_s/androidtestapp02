﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Android.Graphics;


using AndroidTestApp02.SumAnimatorTrial.Animation;
using AndroidTestApp02.SumAnimatorTrial.DrawableObjects.DrawableObjectVOs;

namespace AndroidTestApp02.SumAnimatorTrial.DrawableObjects
{
    class Circle : View
    {

        CircleVO CircleParams;

        ANDSumAnimationAnimator _sumAnimationAnimator;

        public Circle(Context context, CircleVO circleVO) : base(context)
        {
            CircleParams = circleVO;

            _sumAnimationAnimator = new ANDSumAnimationAnimator();

            return;
        }

        protected override void OnDraw(Canvas canvas)
        {
            base.OnDraw(canvas);
           
            canvas.DrawCircle(CircleParams.CenterX, CircleParams.CenterY, CircleParams.Radius, CircleParams.IneteriorPaint);
            
            _sumAnimationAnimator.AnimateMove(this, 500f, 600f);

            return;

        }



    }
}